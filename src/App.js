import React from 'react';
import { Provider } from 'react-redux';
import { ErrorBoundary } from 'react-error-boundary';
import Routes from 'routes/Routes';
// globalization
import { IntlProvider } from 'react-intl';
import {
  createGenerateClassName,
  jssPreset,
  StylesProvider,
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';
import { create } from 'jss';
import jssExtend from 'jss-plugin-extend';
import ErrorFallback from 'error/ErrorFallback';
import Auth from 'auth/Auth';

import muiTheme from 'styles/theme/theme_config.json';

//duck store
import store from './store';
import { TenantProvider } from 'context/TenantContext';
import { InstanceProvider } from 'context/InstanceContext';

const jss = create({
  ...jssPreset(),
  plugins: [...jssPreset().plugins, jssExtend()],
  insertionPoint: document.getElementById('jss-insertion-point'),
});

const generateClassName = createGenerateClassName({
  seed: 'cs',
});
const theme = createMuiTheme(muiTheme);

function App() {
  const [state, setState] = React.useState({
    locale: localStorage.getItem('locale') || 'en',
    message: localStorage.getItem('language'),
  });

  return (
    <StylesProvider jss={jss} generateClassName={generateClassName}>
      <MuiThemeProvider theme={theme}>
        <Provider store={store}>
          <IntlProvider
            locale={state.locale}
            messages={state.messages}
            defaultLocale={'en'}
          >
            <Auth>
              <ErrorBoundary FallbackComponent={ErrorFallback}>
                <TenantProvider>
                  <InstanceProvider>
                    <Routes />
                  </InstanceProvider>
                </TenantProvider>
              </ErrorBoundary>
            </Auth>
          </IntlProvider>
        </Provider>
      </MuiThemeProvider>
    </StylesProvider>
  );
}

export default App;
