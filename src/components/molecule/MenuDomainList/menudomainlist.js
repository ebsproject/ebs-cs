import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
// CORE COMPONENTS AND ATOMS TO USE
import { ListItem, ListItemText, Icon, ListItemIcon } from '@material-ui/core';
import { useQuery } from '@apollo/client';
import { FIND_INSTANCE } from 'utils/apollo/gql/tenant';
import { TenantContext } from 'context/TenantContext';
//MAIN FUNCTION

const useStyles = makeStyles((theme) => ({
  imageIcon: {
    display: 'flex',
    height: 'inherit',
    width: 'inherit',
  },
  imageIcon: {
    height: '100%',
  },
}));

const MenuDomainListMolecule = React.forwardRef((props, ref) => {
  //redux
  const dispatch = useDispatch();
  const classes = useStyles();
  const history = useHistory();

  const tenantContext = useContext(TenantContext);
  const { tenantSelectedById, intanceSelectedById, domainSelectedById } =
    tenantContext.tenantState;

  const { loading, error, data } = useQuery(FIND_INSTANCE, {
    variables: { id: intanceSelectedById },
  });

  if (loading) return null;
  if (error) return `Error! ${error}`;

  const instances = data.findInstance.domaininstances;

  //events
  const handleClickDomain = (item) => {
    try {
      const { hostname, port, protocol } = new URL(item.context);

      if (hostname === window.location.hostname && item.mfe) {
        tenantContext.setTenantState({
          tenantId: Number(tenantSelectedById),
          instanceId: Number(intanceSelectedById),
          domainId: Number(item.domain.id),
        });
        const trfDomain = item.domain.name
          .split(' ')
          .map((word) => word.toLowerCase().charAt(0))
          .join('');

        history.push(`/${trfDomain}`);
      } else {
        window.open(item.context, '__blank');
      }
    } catch {
      alert("The component don't have url");
    }
  };

  return (
    <>
      {instances
        .slice()
        .sort(function (a, b) {
          return a.id - b.id;
        })
        .filter(function (item) {
          return item.domain.id !== domainSelectedById;
        })
        .map((r) => (
          <ListItem button key={r.id} onClick={(event) => handleClickDomain(r)}>
            <ListItemIcon>
              <Icon
                classes={{ root: classes.iconRoot }}
                color='inherit'
                aria-label='open drawer'
                edge='start'
              >
                <img
                  className={classes.imageIcon}
                  src={`/assets/images/domains/${r.domain.icon}`}
                />
              </Icon>
            </ListItemIcon>
            <ListItemText primary={r.domain.name}></ListItemText>
          </ListItem>
        ))}
    </>
  );
});
// Type and required properties
MenuDomainListMolecule.propTypes = {};
// Default properties
MenuDomainListMolecule.defaultProps = {};

export default MenuDomainListMolecule;
