import React from 'react';
// Component to be Test
import ModifyReportButton from './modifyreportbutton';
// Test Library
import { render, cleanup } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import {TenantProvider} from 'context/TenantContext';

afterEach(cleanup);

// * Props to send component to be rendered
const props = {
  rowData: {},
  refresh: () => {},
};

test('ModifyReportButton', () => {
  const { getByTestId } = render(
    <TenantProvider><ModifyReportButton {...props}/></TenantProvider>,
  );
  expect(getByTestId('ModifyReportButtonTestId')).toBeInTheDocument();
});
