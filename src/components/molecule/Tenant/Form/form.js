import React from 'react';
import PropTypes from 'prop-types';
import { useForm, Form } from 'components/atoms/UserForm';
import { makeStyles } from '@material-ui/core/styles';
// CORE COMPONENTS AND ATOMS TO USE
import { useDispatch } from 'react-redux';
import { showMessage } from 'store/ducks/message';
import moment from 'moment';
import {
  Grid,
  Select,
  FormControl,
  InputLabel,
  TextField,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Avatar,
  Typography,
  Icon,
  Switch,
  Divider,
  MenuItem,
} from '@material-ui/core';

import { useQuery, useMutation } from '@apollo/client';
import {
  QUERY_ROWS,
  CREATE_TENANT,
  CREATE_INSTANCE,
  CREATE_DOMAININSTANCE,
} from 'utils/apollo/gql/tenant';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  imageIcon: {
    height: '100%',
  },
  imageIcon: {
    display: 'flex',
    height: 'inherit',
    width: 'inherit',
  },
}));

const initialFValues = {
  id: 0,
  name: '',
  organizationId: 1,
  customerId: 1,
};

const FormMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const dispatch = useDispatch();
  const { open, handleClose, refresh, ...rest } = props;
  const classes = useStyles();
  const [dense, setDense] = React.useState(false);
  const [switchDomain, setSwitchDomain] = React.useState({
    1: true,
    2: true,
  });
  const [errorContext, setErrorContext] = React.useState([]);
  const [errorSgContext, setErrorSgContext] = React.useState([]);
  const [domainContext, setDomainContext] = React.useState([]);
  const [context, setContext] = React.useState([]);
  const [sgContext, setSgContext] = React.useState([]);

  const [domainSelected, setDomainSelected] = React.useState([]);

  const { values, setValues, errors, setErrors, handleInputChange, resetForm } =
    useForm(initialFValues, true, true);

  const [createTenant, { data: dataTenant }] = useMutation(CREATE_TENANT);
  const [createInstance, { data: dataInstance }] = useMutation(CREATE_INSTANCE);
  const [createDomainInstance, { data: dataDomainInstance }] =
    useMutation(CREATE_DOMAININSTANCE);

  //query domainlist
  const {
    loading: loadingDomain,
    error: errorDomain,
    data: Domains,
  } = useQuery(QUERY_ROWS('Domain', 'content { id name info icon}'), {
    variables: { size: 100, number: 1 },
  });

  const {
    loading: loadingOrg,
    error: errorOrg,
    data: OrgsData,
  } = useQuery(QUERY_ROWS('Organization', 'content { id name}'), {
    variables: { size: 100, number: 1 },
  });

  const {
    loading: loadingCust,
    error: errorCust,
    data: CustData,
  } = useQuery(QUERY_ROWS('Customer', 'content { id name}'), {
    variables: { size: 100, number: 1 },
  });

  if (loadingDomain || loadingOrg || loadingCust) return null;
  if (errorDomain) return `Error! ${errorDomain}`;

  //events
  const handleSubmit = (e) => {
    e.preventDefault();
    const customerId = values.customerId;
    const organizationId = values.organizationId;

    const custInfo = CustData.findCustomerList.content;
    const orgInfo = OrgsData.findOrganizationList.content;

    const nameCustomer = custInfo.find((x) => x.id === customerId);
    const nameOrganization = orgInfo.find((x) => x.id === organizationId);

    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth();
    const day = currentDate.getDate();
    const expirationDate = new Date(year + 5, month, day);
    const format = 'YYYY-MM-DD';
    const dateTime = moment(expirationDate).format(format);

    values['name'] = `${nameOrganization ? nameOrganization.name : 'Default'}-${
      nameCustomer ? nameCustomer.name : 'Default'
    }`;
    values['expiration'] = dateTime;
    values['organizationId'] = Number(values.organizationId);
    values['customerId'] = Number(values.customerId);

    createTenant({ variables: { type: values } })
      .then((res) => {
        dispatch(
          showMessage({
            message: `The Tenant ${values.name} was added successfully`,
            variant: 'success',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
          }),
        );

        const tenantId = res.data.createTenant.id;

        for (const key in switchDomain) {
          if (switchDomain[key]) {
            const infoDomain = {
              id: 0,
              mfe: true,
              tenantId: tenantId,
              domainId: key,
              context: '',
              sgContext: '',
            };

            for (const ctx_key in context) {
              if (key === ctx_key) {
                infoDomain.context = context[ctx_key];
                for (const sg_ctx_key in sgContext) {
                  if (ctx_key === sg_ctx_key) {
                    infoDomain.sgContext = sgContext[ctx_key];
                    createDomainInstance({ variables: { type: infoDomain } });
                  }
                }
              }
            }
          }
        }

        dispatch(
          showMessage({
            message: `The Tenant ${values.name} was added successfully`,
            variant: 'success',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
          }),
        );

        resetForm();
        handleClose();
        refresh();
      })
      .catch((err) => {
        dispatch(
          showMessage({
            message: `${err}`,
            variant: 'error',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
          }),
        );
      });
  };

  const handleDomainChange = (event) => {
    setSwitchDomain({ ...switchDomain, [event.target.name]: event.target.checked });
  };

  const handleContextChange = (event) => {
    event.persist();
    const regex =
      /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regex.test(event.target.value)) {
      setContext((previousValues) => ({
        ...previousValues,
        [event.target.id]: event.target.value,
      }));
      setErrorContext({ ...errorContext, [event.target.id]: false });
    } else {
      setErrorContext({ ...errorContext, [event.target.id]: true });
    }
  };

  const handleSgContextChange = (event) => {
    event.persist();
    const regex =
      /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regex.test(event.target.value)) {
      setSgContext((previousValues) => ({
        ...previousValues,
        [event.target.id]: event.target.value,
      }));
      setErrorSgContext({ ...errorSgContext, [event.target.id]: false });
    } else {
      setErrorSgContext({ ...errorSgContext, [event.target.id]: true });
    }
  };

  return (
    <Form
      open={open}
      onSubmit={handleSubmit}
      handleClose={handleClose}
      title={'New Tenant'}
      description={'Register a new tenant'}
    >
      <Grid container>
        <Grid item xs={6}>
          <FormControl required className={classes.formControl}>
            <InputLabel id='organization-label'>Organization</InputLabel>
            <Select
              name='organizationId'
              labelId='organization-label'
              id='org-select'
              value={values.organizationId}
              onChange={(e) => {
                const { name, value } = e.target;
                setValues((switchDomain) => ({
                  ...switchDomain,
                  organizationId: value,
                }));
              }}
            >
              {OrgsData.findOrganizationList.content.map((org) => (
                <MenuItem key={org.id} value={org.id}>
                  {org.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <FormControl required className={classes.formControl}>
            <InputLabel id='cust-label'>Customer</InputLabel>
            <Select
              name='customerId'
              labelId='cust-label'
              id='cust-select'
              value={values.customerId}
              onChange={(e) => {
                const { name, value } = e.target;
                setValues((switchDomain) => ({
                  ...switchDomain,
                  customerId: value,
                }));
              }}
            >
              {CustData.findCustomerList.content.map((cust, index) => (
                <MenuItem key={cust.id} value={cust.id}>
                  {cust.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <Divider />
        </Grid>
        <Grid item xs={12}>
          <Typography variant='h6'>Select the EBS Services/Components</Typography>
          <List dense={dense}>
            {Domains.findDomainList.content
              .slice()
              .sort(function (a, b) {
                return a.id - b.id;
              })
              .map((domain, index) => (
                <>
                  <ListItem>
                    <ListItemAvatar>
                      <Avatar>
                        <Icon
                          classes={{ root: classes.iconRoot }}
                          color='inherit'
                          aria-label='open drawer'
                          edge='start'
                        >
                          <img
                            className={classes.imageIcon}
                            alt='domain'
                            src={`/assets/images/domains/${domain.icon}`}
                          />
                        </Icon>
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={domain.name} secondary={domain.info} />
                    <ListItemSecondaryAction>
                      <Switch
                        key={domain.id}
                        checked={
                          switchDomain[domain.id] ? switchDomain[domain.id] : false
                        }
                        name={domain.id}
                        onChange={handleDomainChange}
                      />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <FormControl
                    error={errorContext[domain.id] ? true : false}
                    fullWidth
                    className={classes.margin}
                  >
                    <TextField
                      required
                      error={errorContext[domain.id] ? true : false}
                      helperText={errorContext[domain.id] ? 'Not Valid URL' : null}
                      onChange={handleContextChange}
                      key={domain.id}
                      id={domain.id}
                      label='Context'
                    />
                    <TextField
                      required
                      error={errorSgContext[domain.id] ? true : false}
                      helperText={errorSgContext[domain.id] ? 'Not Valid URL' : null}
                      onChange={handleSgContextChange}
                      key={domain.id}
                      id={domain.id}
                      label='Service gateway context'
                    />
                  </FormControl>
                </>
              ))}
          </List>
        </Grid>
        <Grid item xs={12}></Grid>
      </Grid>
    </Form>
  );
});
// Type and required properties
FormMolecule.propTypes = {
  handleClose: PropTypes.object,
  open: PropTypes.bool,
};
// Default properties
FormMolecule.defaultProps = {
  handleClose: null,
  open: false,
};

export default FormMolecule;
