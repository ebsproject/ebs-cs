import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import {useSelector, useDispatch} from 'react-redux';
import {Grid, Typography} from '@material-ui/core';
import {useQuery} from '@apollo/client';
import {FIND_TENANT} from 'utils/apollo/gql/tenant';
import {FormattedMessage} from 'react-intl';
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const TenantInfoMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const {tenantId} = props;
  const {loading, error, data} = useQuery(FIND_TENANT, {
    variables: {id: tenantId},
  });
  const userInfo = useSelector(({user}) => user);

  if (error) throw error;
  if (loading) return null;
  if (!userInfo) return null;

  const userName = userInfo.userName;
  const tenant = data.findTenant;

  return (
    <Grid container direction="row" justify="center" alignItems="center">
      <Grid item xs={12}>
        <Typography variant="h6">
          <FormattedMessage
            id="tnt.comp.comsoon.welcome"
            defaultMessage={`Your Account ${
              userName
            } is part of the of`}
          />
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography align="center">
          {`Customer Name: ${tenant.customer.name}`}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography align="center">
          {`Organization Name: ${tenant.organization.name}`}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography align="center">
          {`Admin Contact: ${tenant.customer.officialEmail}`}
        </Typography>
      </Grid>
    </Grid>
  );
});
// Type and required properties
TenantInfoMolecule.propTypes = {
  tenantId: PropTypes.number.isRequired,
};
// Default properties
TenantInfoMolecule.defaultProps = {
  tenantId: 0,
};

export default TenantInfoMolecule;
