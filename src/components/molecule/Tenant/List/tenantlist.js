import React from 'react';
import EbsGrid from 'ebs-grid-lib';
import { transform } from 'node-json-transform';
import { QUERY_ROWS } from 'utils/apollo/gql/tenant';
import client from 'utils/apollo';
import { showMessage } from 'store/ducks/message';
import { useDispatch } from 'react-redux';

import { ToolbarActionAdd } from 'components/atoms/ToolbarActionAdd';
import ToolbarActionEdit from 'components/molecule/ModifyTenantButton';
import { ToolbarActionDelete } from 'components/atoms/ToolbarActionDelete';
// import { ToolbarActionPrint } from 'components/atoms/ToolbarActionPrint';

const TenantListMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const [result, setResult] = React.useState(false);
  const [page, setPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const dispatch = useDispatch();

  function fetch(page, pageSize, columnsToFilter, value) {
    const map = {
      item: {
        id: 'id',
        name: 'name',
        expiration: 'expiration',
        customerId: 'customer.id',
        customerName: 'customer.name',
        customerLogo: 'customer.logo',
        organizationId: 'organization.id',
        organizationName: 'organization.name',
        organizationLogo: 'organization.logo',
        organizationLegalName: 'organization.legalName',
      },
    };
    client
      .query({
        query: QUERY_ROWS(
          'Tenant',
          'content { id name expiration customer { id name logo }  organization { id name logo legalName } }',
        ),
        variables: {
          size: pageSize,
          number: page,
        },
        fetchPolicy: 'no-cache',
      })
      .then(({ data }) => {
        const result = transform(data.findTenantList.content, map);
        setPage(page);
        setTotalPages(data.findTenantList.totalPages);
        setResult(result);
      })
      .catch(({ message }) => {
        dispatch(
          showMessage({
            message: message,
            variant: 'error',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
          }),
        );
      });
  }

  const columns = [
    { Header: 'id', accessor: 'id', hidden: true },
    { Header: 'Registration ID', accessor: 'name', filter: true },
    { Header: 'Organization', accessor: 'organizationName', filter: true },
    { Header: 'Legal Name', accessor: 'organizationLegalName', filter: true },
    { Header: 'Logo', accessor: 'organizationLogo', filter: false },
    { Header: 'Customer', accessor: 'customerName', filter: true },
    { Header: 'Customer Logo', accessor: 'customerLogo', filter: false },
    { Header: 'Expiration', accessor: 'expiration', filter: true },
  ];

  const toolbarActions = (selection, refresh) => {
    return (
      <>
        <ToolbarActionAdd refresh={refresh} />
        <ToolbarActionDelete selection={selection} refresh={refresh} />
        {/* <ToolbarActionPrint selection={selection} refresh={refresh} /> */}
      </>
    );
  };

  const rowActions = (rowData, refresh) => {
    return (
      <>
        <ToolbarActionEdit rowData={rowData} refresh={refresh} />
      </>
    );
  };

  return (
    <EbsGrid
      toolbar={true}
      columns={columns}
      data={result}
      title={'Registers'}
      fetch={fetch}
      rowactions={rowActions}
      toolbaractions={toolbarActions}
      select='single'
      page={page}
      totalPages={totalPages}
      pagination
    />
  );
});
// Type and required properties
TenantListMolecule.propTypes = {};
// Default properties
TenantListMolecule.defaultProps = {};

export default TenantListMolecule;
