import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import {Grid, Typography, Button} from '@material-ui/core';
import CardTenant from 'components/atoms/CardTenant';
import {FormattedMessage} from 'react-intl';
import {TenantContext} from 'context/TenantContext';

const TenantListOrganism = React.forwardRef((props, ref) => {
  const {tenants} = props;
  const tenantContext = useContext(TenantContext);

  const handleCardClick = (e) => {
    e.preventDefault();
    tenantContext.setTenantState({tenantId: Number(e.currentTarget.id)});
  };

  //determinate if a tenant has only one tenant
  if (tenants != null && tenants.length === 1) {
    tenantContext.setTenantState({
      tenantId: Number(tenants[0].id),
      instanceId: null,
      domainId: null,
    });
  }

  return (
    <Grid
      container
      direction="row"
      justify="flex-start"
      alignItems="flex-start"
      data-testid={'TenantListTestId'}
      spacing={6}
    >
      <Grid item xs={12}>
        <Typography variant="h4">
          <FormattedMessage
            id="tnt.comp.comsoon.welcome"
            defaultMessage="Select an organization"
          />
        </Typography>
      </Grid>

      {tenants
        .slice()
        .sort(function (a, b) {
          return a.id - b.id;
        })
        .map((tenant, index) => (
          <Grid key={tenant.name} item xs={4}>
            <CardTenant key={index} ref={ref} tenantId={Number(tenant.id)}>
              <Button
                size="small"
                color="primary"
                id={tenant.id}
                onClick={handleCardClick}
              >
                <FormattedMessage
                  id="tnt.tenant.select"
                  defaultMessage="Select"
                />
              </Button>
            </CardTenant>
          </Grid>
        ))}
    </Grid>
  );
});
// Type and required properties
TenantListOrganism.propTypes = {
  tenants: PropTypes.array,
  children: PropTypes.node,
};
// Default properties
TenantListOrganism.defaultProps = {
  tenants: [],
  children: null,
};

export default TenantListOrganism;
