import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
// CORE COMPONENTS AND ATOMS TO USE
import {
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Button,
} from '@material-ui/core';
import {useQuery} from '@apollo/client';
import {FIND_TENANT} from 'utils/apollo/gql/tenant';
import {FormattedMessage} from 'react-intl';
import {TenantContext} from 'context/TenantContext';

const useStyles = makeStyles((theme) => ({
  root: {},
  table: {
    minWidth: 650,
  },
  tableCellHeader: {
    background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  tableCellTh: {
    fontWeight: 'bold',
  },
}));

const TenantInstanceInfoMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const {tenantId} = props;
  const classes = useStyles();
  const tenantContext = useContext(TenantContext);

  const {loading, error, data} = useQuery(FIND_TENANT, {
    variables: {id: tenantId},
  });

  const handleInstanceClick = (e) => {
    e.preventDefault();
    tenantContext.setTenantState({
      tenantId: tenantId,
      instanceId: Number(e.currentTarget.id),
    });
  };

  if (error) throw error;
  if (loading) return null;

  const instances = data.findTenant.instances;

  // determinate if a tenant has only one instance
  // if (instances.length === 1) {
  //   tenantContext.setTenantState({
  //     tenantId: tenantId,
  //     instanceId: Number(instances[0].id),
  //   });
  // }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell
              align="center"
              colSpan={4}
              className={classes.tableCellHeader}
            >
              <Typography variant="h6" align="center">
                <FormattedMessage
                  id="tnt.comp.comsoon.welcome"
                  defaultMessage="This tenant selected has differents  EBS Instances deployed in your institution"
                />
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCellTh}>EBS Instance</TableCell>
            <TableCell align="center" className={classes.tableCellTh}>
              Server
            </TableCell>
            <TableCell align="center" className={classes.tableCellTh}>
              Port
            </TableCell>
            <TableCell align="center" className={classes.tableCellTh}>
              Actions
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {instances
            .slice()
            .sort(function (a, b) {
              return a.id - b.id;
            })
            .map((row) => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="center">{row.server}</TableCell>
                <TableCell align="center">{row.port}</TableCell>
                <TableCell align="center">
                  <Button
                    size="small"
                    color="primary"
                    id={row.id}
                    onClick={handleInstanceClick}
                  >
                    <FormattedMessage
                      id="tnt.tenant.select"
                      defaultMessage="View the Products"
                    />
                  </Button>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
});
// Type and required properties
TenantInstanceInfoMolecule.propTypes = {
  tenantId: PropTypes.number.isRequired,
};
// Default properties
TenantInstanceInfoMolecule.defaultProps = {
  tenantId: PropTypes.number.isRequired,
};

export default TenantInstanceInfoMolecule;
