import React from 'react';
// Component to be Test
import AddReportButton from './addreportbutton';
// Test Library
import userEvent from '@testing-library/user-event';
import {
  render,
  cleanup,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { TenantProvider } from 'context/TenantContext';
import client from 'utils/apollo';
import store from 'store';
import { ApolloProvider } from '@apollo/client';
import { Provider } from 'react-redux';
import { ErrorBoundary } from 'react-error-boundary';
import ErrorFallback from 'error/ErrorFallback';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import { setupServer } from 'msw/node';
import { handlers } from 'utils/test/handlers';

const graphqlServer = setupServer(...handlers);

afterAll(() => graphqlServer.close());
afterEach(cleanup);
beforeAll(() => graphqlServer.listen());

const Providers = ({ children }) => (
  <IntlProvider locale='en' messages={{}} defaultLocale='en'>
    <TenantProvider>
      <ErrorBoundary FallbackComponent={ErrorFallback}>
        <Provider store={store}>
          <ApolloProvider client={client}>{children}</ApolloProvider>
        </Provider>
      </ErrorBoundary>
    </TenantProvider>
  </IntlProvider>
);
const props = {
  refresh: () => {},
  handleMenuClose: () => {},
};

test('AddReportButton is in the DOM', () => {
  render(<AddReportButton {...props} />, {
    wrapper: Providers,
  });
  expect(screen.getByTestId('AddReportButtonTestId')).toBeInTheDocument();
});

test('AddReportButton opens dialog and mounts all components', async () => {
  render(<AddReportButton {...props} />, {
    wrapper: Providers,
  });
  expect(screen.getByTestId('AddReportButtonTestId')).toBeInTheDocument();
  // * click on the AddReportbutton
  userEvent.click(screen.getByRole('menuitem', { name: 'New Empty Report' }));
  // * Open dialog
  expect(screen.getByRole('dialog')).toBeInTheDocument();
  // * select program
  expect(screen.getByRole('textbox', { name: /programs/i })).toBeInTheDocument();
  userEvent.click(screen.getByRole('textbox', { name: /programs/i }));
  // * select program
  expect(screen.getByRole('textbox', { name: /products/i })).toBeInTheDocument();
  userEvent.click(screen.getByRole('textbox', { name: /products/i }));
  // * Type name
  expect(screen.getByTestId('name')).toBeInTheDocument();
  userEvent.type(screen.getByTestId('name'), 'TestReport');
  expect(screen.getByTestId('zpl')).toBeInTheDocument();
  userEvent.type(screen.getByTestId('zpl'), 'N/A');
  expect(screen.getByTestId('description')).toBeInTheDocument();
  userEvent.type(screen.getByTestId('description'), 'Test description');
  expect(screen.getByRole('button', { name: /save/i })).toBeInTheDocument();
  expect(screen.getByRole('button', { name: /close/i })).toBeInTheDocument();
  userEvent.click(screen.getByRole('button', { name: /close/i }));
  await waitForElementToBeRemoved(() => screen.getByRole('dialog'))
});
