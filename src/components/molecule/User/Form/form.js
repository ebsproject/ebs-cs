import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useForm, Form } from 'components/atoms/UserForm'
import { makeStyles } from '@material-ui/core/styles';
// CORE COMPONENTS AND ATOMS TO USE
import { useDispatch, useSelector } from 'react-redux';
import { hideMessage, showMessage } from 'store/ducks/message';
import {
  Grid, Select, FormControl, InputLabel, Button,
  List, ListItem, ListItemAvatar, ListItemIcon, ListItemSecondaryAction,
  ListItemText, Avatar, Typography, Icon, Switch, Divider, MenuItem, Input, ButtonGroup, Checkbox, FormControlLabel
} from '@material-ui/core'

import { useQuery, useMutation } from "@apollo/client";
import { CREATE_PERSON, CREATE_USER } from "utils/apollo/gql/user";
import { QUERY_ROWS } from "utils/apollo/gql/tenant";
import * as yup from 'yup';
import axios from 'axios'
import SelectAlt from "react-select";
import { transform, transformAsync } from "node-json-transform";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  imageIcon: {
    height: "100%",
  },
  imageIcon: {
    display: "flex",
    height: "inherit",
    width: "inherit",
  },
}));



const initialFValues = {
  id: 0,
  userName: "",
  userPassword: "",
  isIs: true,
  person: {
    id: 0,
    familyName: "",
    givenName: "",
    officialEmail: "",
    email: "",
    gender: "M",
    hasCredential: true,
    jobTitle: "",
    knowsAbout: "",
    language: {
      id: 1
    },
    tenant: {
      id: 1
    },
    personstatus: {
      id: 1
    }
  }
}


const FormMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const dispatch = useDispatch();
  const { open, handleClose, ...rest } = props
  const [userTenants, setUserTenants] = React.useState([]);
  const classes = useStyles();

  const {
    loading: loadingTenant,
    error: errorTenant,
    data: dataTenant,
  } = useQuery(QUERY_ROWS("Tenant", "content { id name  }"), {
    variables: { size: 100, number: 1 },
  });
  const [createPerson, { data }] = useMutation(CREATE_PERSON);
  const [createUser, { data: dataUser }] = useMutation(CREATE_USER);

  const {
    values,
    setValues,
    errors,
    setErrors,
    handleInputChange,
    resetForm
  } = useForm(initialFValues, true, true);


  if (loadingTenant) return <p>Test</p>;
  if (errorTenant)
    return (
      <p>
        {errorTenant}

      </p>
    );

  const mapChip = {
    item: {
      value: "id",
      label: "name",
    },
  };

  const resultTenant = transform(dataTenant.findTenantList.content, mapChip);



  const handleInputChangeLocal = e => {
    const { name, value } = e.target
    setValues({
      ...values,
      person: {
        ...values.person,
        [name]: value
      }
    })
    /* if (validateOnChange)
         validate({ [name]: value })*/
  }
  const handleSubmit = e => {
    e.preventDefault()
    values["isIs"] = values.isIs ? 1 : 0;
    createPerson({ variables: { type: values.person } }).then(r => {
      const personId = r.data.createPerson.id;
      values.person["id"] = personId
      createUser({ variables: { type: values } }).then(resultUser => {

        userTenants.map(item => {

          const dataUserTenant = {

            "userid": resultUser.data.createUser.id,
            "tenantid": item.value

          }
console.log(dataUserTenant);
          axios.post(
            `${process.env.REACT_APP_CSAPI_URI_REST}cs/addUserToTenant`,dataUserTenant
          ).then(resulta => {
            console.log(resulta);
          })
        })



        dispatch(
          showMessage({
            message: `The User ${values.person.givenName} was added successfully`,
            variant: 'success',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right'
            }
          })
        )

      }).catch(err => {
        dispatch(
          showMessage({
            message: ` ${err}`,
            variant: 'error',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right'
            }
          })
        )
      })


      // resetForm()
      // handleClose()
    })
  }

  const handleGetProfile = e => {


    axios.post(`${process.env.REACT_APP_CSAPI_URI_REST}services/SyncProfile`, {
      "username": values.userName
    }, {
      crossdomain: true,
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    }).then(result => {
      const profile = result.data;
      setValues(state => ({
        ...state,
        person: {
          ...values.person,
          givenName: profile.givenName,
          jobTitle: profile.jobTitle,
          email: profile.mail,
          familyName: profile.displayName
        }
      }))

    }).catch(err => {
      console.log(err);
    })
  }

  const onChangeTenant = (value, { action, removedValue }) => {

    setUserTenants(value);

  };
  return (
    <Form open={open} onSubmit={handleSubmit} handleClose={handleClose} title={"New User"} description={"Register a new User"}>
      <Grid container>
        <Grid item xs={6}>
          <FormControlLabel
            control={<Checkbox checked={values.isIs} onChange={handleInputChange} name="isIs" />}
            label="Organization Account"
          />
          <FormControl>
            <InputLabel htmlFor="email_field">User Account</InputLabel>
            <Input
              id="email_field"
              name="userName" value={values.userName} onChange={handleInputChange}
            />
          </FormControl>
          <ButtonGroup>
            <Button onClick={handleGetProfile}>Get Profile</Button>
          </ButtonGroup>
          <FormControl>
            <InputLabel htmlFor="pwd_field">Password</InputLabel>
            <Input
              id="pwd_field"
              name="userPassword" inputProps={{ readOnly: values.isIs }} type="password" value={values.userPassword} onChange={handleInputChange}
            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="givenName_field">Given Name</InputLabel>
            <Input
              id="givenName_field"
              name="givenName" value={values.person.givenName} onChange={handleInputChangeLocal}
            />
          </FormControl>

          <FormControl>
            <InputLabel htmlFor="familyName_field">Given Name</InputLabel>
            <Input
              id="familyName_field"
              name="familyName" value={values.person.familyName} onChange={handleInputChangeLocal}
            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="jobTitle_field">Job</InputLabel>
            <Input
              id="jobTitle_field"
              name="jobTitle" value={values.person.jobTitle} onChange={handleInputChangeLocal}
            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="email_field">Email</InputLabel>
            <Input
              id="email_field"
              name="email" value={values.person.email} onChange={handleInputChangeLocal}
            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="tenantSelect">Tenant</InputLabel>
            <SelectAlt
              onChange={onChangeTenant}
              closeMenuOnSelect={false}
              isMulti
              options={resultTenant}
              inputId={"tenantSelect"}
            />
          </FormControl>


        </Grid>
      </Grid>
    </Form>
  )
})
// Type and required properties
FormMolecule.propTypes = {
  handleClose: PropTypes.object,
  open: PropTypes.bool,
}
// Default properties
FormMolecule.defaultProps = {
  handleClose: null,
  open: false,
}

export default FormMolecule
