import {useForm} from 'react-hook-form';
import PropTypes from 'prop-types';
import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button,
  TextField,
  Grid,
  FormControl,
} from '@material-ui/core';
import axios from 'axios';

import {Send} from '@material-ui/icons';
import {transform} from 'node-json-transform';
import {useQuery} from '@apollo/client';
import {GET_USER_BY_ID} from 'utils/apollo/gql/user';
import {QUERY_ROWS} from 'utils/apollo/gql/tenant';
import Select from 'react-select';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  formControl: {
    marginTop: theme.spacing(2),
    minHeight: 450,
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const UserProfile = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const {open, handleClose, userName} = props;
  const {handleSubmit} = useForm();
  const [fullWidth, setFullWidth] = React.useState(true);
  const [maxWidth, setMaxWidth] = React.useState('sm');
  const [userTenants, setUserTenants] = React.useState([]);
  const [userRoles, setUserRoles] = React.useState([]);
  const [userFuncUnits, setUserFuncUnits] = React.useState([]);

  const onSubmit = (data) => console.log(data);

  const {
    loading: loadingProfile,
    error: errorProfile,
    data: dataProfile,
  } = useQuery(GET_USER_BY_ID, {
    variables: {id: userName},
  });

  const {
    loading: loadingTenant,
    error: errorTenant,
    data: dataTenant,
  } = useQuery(QUERY_ROWS('Tenant', 'content { id name  }'), {
    variables: {size: 100, number: 1},
  });

  const {loading: loadingRoles, error: errorRoles, data: dataRoles} = useQuery(
    QUERY_ROWS('Role', 'content { id name tenant }'),
    {
      variables: {size: 100, number: 1},
    }
  );

  const {
    loading: loadingFuncUnits,
    error: errorFuncUnits,
    data: dataFuncUnits,
  } = useQuery(QUERY_ROWS('FunctionalUnit', 'content { id name tenant }'), {
    variables: {size: 100, number: 1},
  });

  if (loadingProfile || loadingTenant || loadingRoles || loadingFuncUnits)
    return <p>Test</p>;
  if (errorTenant || errorProfile || errorRoles || errorFuncUnits)
    return (
      <p>
        {errorTenant}
        {errorProfile}
      </p>
    );

  const map = {
    item: {
      id: 'id',
      name: 'userName',
      familyName: 'person.familyName',
      givenName: 'person.givenName',
      jobTitle: 'person.jobTitle',
    },
  };

  const mapChip = {
    item: {
      value: 'id',
      label: 'name',
    },
  };

  const resultProfile = transform(dataProfile.findUser, map);
  const resultTenant = transform(dataTenant.findTenantList.content, mapChip);
  const resultRole = transform(dataRoles.findRoleList.content, mapChip);
  const resultFuncUnit = transform(
    dataFuncUnits.findFunctionalUnitList.content,
    mapChip
  );

  if (resultProfile.length === 0) return 'ErrorA';
  if (resultTenant.length === 0) return 'ErrorB';

  const resultUserTenants = transform(dataProfile.findUser.tenants, mapChip);
  const resultUserRole = transform(dataProfile.findUser.roles, mapChip);

  const resultUserFuncUnit = transform(
    dataProfile.findUser.functionalunits,
    mapChip
  );

  //event
  const onChangeTenant = (value, { action, removedValue }) => {
    axios.post(
      `${process.env.REACT_APP_CSAPI_URI_REST}cs/updateTenantUser`,{
        "userid": userName,       
      }
    ).then(r =>{
      value.map(item=>{
        axios.post(
          `${process.env.REACT_APP_CSAPI_URI_REST}cs/addUserToTenant`,{
            "userid": userName,
            "tenantid": item.value
          }
        ).then(r =>{
          
        })
      })
    })
    
   

    
  };
  const onChangeRole = (value, { action, removedValue }) => {

    /*axios.post(
      `${process.env.REACT_APP_CSAPI_URI_WFREST}api/user/${userName}/roles`,value
    ).then(r =>{
      setUserRoles(value);
    })*/
  
  };

  const onChangeFuncUnit = (value, { action, removedValue }) => {
    /*axios.post(
      `${process.env.REACT_APP_CSAPI_URI_WFREST}api/user/${userName}/funcunit`,value
    ).then(r =>{
      setUserFuncUnits(value);
    })*/
    
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="userProfileDialog"
      open={open}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
    >
      <DialogTitle id="userProfileDialog">User Profile</DialogTitle>
      <DialogContent>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className={classes.root}
          noValidate
          autoComplete="off"
        >
          <FormControl className={classes.formControl}>
            <Grid container spacing={1}>
              <Grid item xs={6}>
                <TextField label={'User Name'} value={resultProfile.name} />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label={'Given Name'}
                  value={resultProfile.givenName}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label={'Family Name'}
                  value={resultProfile.familyName}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField label={'Title'} value={resultProfile.jobTitle} />
              </Grid>

              <Grid item xs={12}>
                <label htmlFor="tenantSelect">Tenant</label>
                <Select
                  onChange={onChangeTenant}
                  closeMenuOnSelect={false}
                  isMulti
                  defaultValue={
                    userTenants.length ? userTenants : resultUserTenants
                  }
                  options={resultTenant}
                  inputId={'tenantSelect'}
                />
              </Grid>
              <Grid item xs={12}>
                <label htmlFor="rolesSelect">Roles</label>
                <Select
                  onChange={onChangeRole}
                  closeMenuOnSelect={false}
                  options={resultRole}
                  inputId={'rolesSelect'}
                  defaultValue={userRoles.length ? userRoles : resultUserRole}
                  isMulti
                />
              </Grid>

              <Grid item xs={12}>
                <label htmlFor="funtionalUnitSelect">Functional Units</label>
                <Select
                  onChange={onChangeFuncUnit}
                  closeMenuOnSelect={false}
                  options={resultFuncUnit}
                  defaultValue={
                    userFuncUnits.length ? userFuncUnits : resultUserFuncUnit
                  }
                  isMulti
                  inputId={'funtionalUnitSelect'}
                />
              </Grid>
            </Grid>
          </FormControl>
        </form>
      </DialogContent>
      <DialogActions>
        <Button
          autoFocus
          onClick={handleClose}
          color="primary"
          startIcon={<Send />}
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
});

UserProfile.propTypes = {
  open: PropTypes.bool.isRequired,
  userName: PropTypes.string.isRequired,
  handleClose: PropTypes.func,
};
export default UserProfile;
