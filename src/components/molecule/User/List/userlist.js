import React from 'react';

import {makeStyles, useTheme, fade} from '@material-ui/core/styles';
import ToolbarActionDefault from 'components/molecule/ToolbarActions';
import FormUser from '../Form';
import UserProfile from './profile';

// CORE COMPONENTS AND ATOMS TO USE
import {IconButton, Tooltip, Switch, Grid} from '@material-ui/core';
import EbsGrid from 'ebs-grid-lib';
import {transform, transformAsync} from 'node-json-transform';
import {useQuery} from '@apollo/client';
import {QUERY_ROWS} from 'utils/apollo/gql/tenant';
import {getCurrentTenant} from 'utils/helpers';

import {AssignmentInd} from '@material-ui/icons';
//MAIN FUNCTION

const UserListMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const [open, setOpen] = React.useState(false);
  const [openP, setOpenP] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState(null);

  const {loading, error, data} = useQuery(
    QUERY_ROWS(
      'User',
      'content { id userName person { familyName  additionalName givenName  jobTitle knowsAbout language { id name }   } }'
    ),
    {
      variables: {size: 100, number: 1},
    }
  );

  if (loading) return null;
  if (error) return `Error! ${error}`;

  const map = {
    item: {
      id: 'id',
      name: 'userName',
      givenName: 'person.givenName',
      familyName: 'person.familyName',
      jobTitle: 'person.jobTitle',
      knowsAbout: 'person.knowsAbout',
      language: 'person.language.name',
    },
    each: function (item) {
      item.fullName = `${item.familyName.toUpperCase()},${item.givenName}`;
      return item;
    },
  };

  //transform data
  const result = transform(data.findUserList.content, map).sort(function (
    a,
    b
  ) {
    return a.id - b.id;
  });

  // actions
  const rowActions = (rowData, refresh) => {
    return (
      <Grid container direction="row">
        <Grid item xs={6}>
          <Tooltip title="View Permissions">
            <IconButton
              aria-label="profile"
              color="primary"
              onClick={() => handleClickOpen(rowData)}
            >
              <AssignmentInd />
            </IconButton>
          </Tooltip>
        </Grid>
        <Grid item xs={6}>
          <Tooltip title="Active">
            <Switch name="enableUser" checked={true} color="primary" />
          </Tooltip>
        </Grid>
      </Grid>
    );
  };

  const columns = [
    {Header: 'id', accessor: 'id', hidden: true},
    {Header: 'User', accessor: 'name', filter: true},
    {Header: 'Full Name', accessor: 'fullName', filter: true},
    {Header: 'Title', accessor: 'jobTitle', filter: true},
    {Header: 'Knows About', accessor: 'knowsAbout', filter: true},
    {Header: 'Primary Language', accessor: 'language', filter: true},
  ];

  const toolbarActions = (selection, refresh) => {
    const handleRefresh = (e) => {
      refresh();
    };
    const handlePrint = (e) => {
      alert('print');
    };
    const handleAdd = (e) => {
      setOpen(true);
    };
    return (
      <ToolbarActionDefault
        handlePrint={handlePrint}
        handleRefresh={handleRefresh}
        handleAdd={handleAdd}
      />
    );
  };

  //events
  const handleClickOpen = (data) => {
    setSelectedValue(data.id);
    setOpenP(true);
  };
  const handleClose = (value) => {
    setOpen(false);
  };
  const handleCloseP = (value) => {
    setOpenP(false);
  };
  return (
    <>
      <EbsGrid
        toolbar={true}
        columns={columns}
        data={result}
        title={'Users'}
        rowactions={rowActions}
        toolbaractions={toolbarActions}
      />
      <FormUser open={open} handleClose={handleClose} />
      {selectedValue && (
        <UserProfile
          userName={selectedValue}
          open={openP}
          handleClose={handleCloseP}
        />
      )}
    </>
  );
});

export default UserListMolecule;
