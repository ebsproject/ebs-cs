import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import EbsGrid from 'ebs-grid-lib';
import {transform} from 'node-json-transform';
import {useQuery} from '@apollo/client';
import {FIND_INSTANCE} from 'utils/apollo/gql/tenant';

//MAIN FUNCTION

const DomainInstancesMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const {instanceId} = props;

  const {loading, error, data} = useQuery(FIND_INSTANCE, {
    variables: {id: instanceId},
  });

  if (loading) return null;
  if (error) return `Error! ${error}`;

  const instanceInfo = data.findInstance.domaininstances;

  const map = {
    item: {
      id: 'id',
      domainId: 'domain.id',
      name: 'domain.name',
      info: 'domain.info',
      webUrl: 'context',
      apiUrl: 'sgContext',
    },
  };

  const result = transform(instanceInfo, map);
  const columns = [
    {Header: 'id', accessor: 'id', hidden: true},
    {Header: 'Domain', accessor: 'name', filter: true},
    {Header: 'Description', accessor: 'info', filter: true},
    {Header: 'URI', accessor: 'webUrl', filter: true},
    {Header: 'SG', accessor: 'apiUrl', filter: true},
  ];

  return (
    <EbsGrid
      toolbar={false}
      columns={columns}
      title={'InstanceDomains'}
      data={result}
    />
  );
});
// Type and required properties
DomainInstancesMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
};
// Default properties
DomainInstancesMolecule.defaultProps = {
  label: 'Hello world',
  children: null,
};

export default DomainInstancesMolecule;
