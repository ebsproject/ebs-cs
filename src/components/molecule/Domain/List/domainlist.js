import React from 'react';

// CORE COMPONENTS AND ATOMS TO USE
import EbsGrid from 'ebs-grid-lib';
import ProductList from 'components/molecule/ProductList';
import NewDomainsMenuButton from 'components/molecule/NewDomainsMenuButton'
import { Box } from '@material-ui/core';
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DomainListMolecule = React.forwardRef(({}, ref) => {
  // Properties of the molecule
  const columns = [
    { Header: 'id', accessor: 'id', hidden: true },
    { Header: 'Name', accessor: 'name', filter: true },
    { Header: 'Description', accessor: 'info', filter: true },
    { Header: 'Icon', accessor: 'icon', filter: true },
  ];

  const toolbarActions = (rowSelected, refresh) => {
    return (
      <Box>
        <Box>
          <NewDomainsMenuButton rowSelected={rowSelected.length > 0 && rowSelected[0].original || null} refresh={refresh} />
        </Box>
      </Box>
    );
  };

  const ProductDetail = (rowData) => {
    return <ProductList domainData={rowData.original} />;
  };

  return (
    <EbsGrid
      toolbar={true}
      columns={columns}
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      entity='Domain'
      title={'Domains'}
      toolbaractions={toolbarActions}
      detailcomponent={ProductDetail}
      callstandard='graphql'
      select="single"
    />
  );
});
// Type and required properties
DomainListMolecule.propTypes = {};
// Default properties
DomainListMolecule.defaultProps = {};

export default DomainListMolecule;
