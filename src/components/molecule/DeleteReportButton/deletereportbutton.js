import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  List,
  ListItemText,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { DeleteForever } from '@material-ui/icons';
import ApolloClient from 'utils/apollo';
import { useDispatch } from 'react-redux';
import { showMessage } from 'store/ducks/message';
import {
  REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS,
  REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS,
  DELETE_PRINTOUT_TEMPLATE,
} from 'utils/apollo/gql/printoutManager';
import { printoutClient } from 'utils/axios';
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DeleteReportButtonMolecule = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    async function handleMutation(reportData) {
      let productIds = [];
      let programIds = [];
      reportData.products.map((product) => productIds.push(Number(product.id)));
      reportData.programs.map((program) => programIds.push(Number(program.id)));
      // *Removing products
      const { error: ErrorRemoveProduct, data: DataRemoveProduct } =
        await ApolloClient.mutate({
          mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS,
          variables: {
            printoutTemplateId: Number(reportData.id),
            productIds: productIds,
          },
        });
      // *Removing programs
      const { error: ErrorRemoveProgram, data: DataRemoveProgram } =
        await ApolloClient.mutate({
          mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS,
          variables: {
            printoutTemplateId: Number(reportData.id),
            programIds: programIds,
          },
        });
      // *Removing Temmplate Metadata from database
      if (!ErrorRemoveProduct && !ErrorRemoveProgram) {
        ApolloClient.mutate({
          mutation: DELETE_PRINTOUT_TEMPLATE,
          variables: {
            printoutTemplateId: Number(reportData.id),
          },
        })
          .then(({ data }) => {
            // ? No errors, delete phisycal report
            printoutClient
              .delete(`api/Report/Delete?name=${reportData.name}`)
              .then((data) => {
                // ? No errors, refresh grid and close dialog
                dispatch(
                  showMessage({
                    message: `Report successfully deleted`,
                    variant: 'success',
                    anchorOrigin: {
                      vertical: 'top',
                      horizontal: 'right',
                    },
                  }),
                );
                setOpen(false);
                refresh();
              })
              .catch(({ message }) => {
                dispatch(
                  showMessage({
                    message: message,
                    variant: 'error',
                    anchorOrigin: {
                      vertical: 'top',
                      horizontal: 'right',
                    },
                  }),
                );
                setOpen(false);
              });
          })
          .catch(({ message }) => {
            dispatch(
              showMessage({
                message: message,
                variant: 'error',
                anchorOrigin: {
                  vertical: 'top',
                  horizontal: 'right',
                },
              }),
            );
          });
      }
    }

    const handleDelete = () => {
      selectedRows.map((report) => {
        handleMutation(report.original);
      });
    };

    return (
      /* 
     @prop data-testid: Id to use inside deletereportbutton.test.js file.
     */
      <div ref={ref} data-testid={'DeleteReportButtonTestId'}>
        <Tooltip
          title={<FormattedMessage id='none' defaultMessage='Delete reports' />}
        >
          <span>
            <IconButton
              aria-label='DeleteReportButton'
              onClick={handleClickOpen}
              color='inherit'
              disabled={selectedRows.length > 0 ? false : true}
            >
              <DeleteForever />
            </IconButton>
          </span>
        </Tooltip>
        <Dialog
          fullWidth
          keepMounted
          maxWidth='sm'
          open={open}
          onClose={handleClose}
        >
          <DialogTitle>
            <FormattedMessage id='none' defaultMessage='Delete reports' />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <FormattedMessage
                id='none'
                defaultMessage='Are you sure to delete follow reports?'
              />
            </DialogContentText>
            <List>
              {selectedRows.map((item, key) => (
                <ListItemText key={key} primary={item.original.name} />
              ))}
            </List>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <Typography variant='button' color='secondary'>
                <FormattedMessage id='none' defaultMessage='Cancel' />
              </Typography>
            </Button>
            <Button onClick={handleDelete}>
              <Typography variant='button'>
                <FormattedMessage id='none' defaultMessage='Confirm' />
              </Typography>
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  },
);
// Type and required properties
DeleteReportButtonMolecule.propTypes = {
  selectedRows: PropTypes.array.isRequired,
  refresh: PropTypes.func.isRequired,
};
// Default properties
DeleteReportButtonMolecule.defaultProps = {};

export default DeleteReportButtonMolecule;
