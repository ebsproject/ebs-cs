import React from 'react';
// Component to be Test
import DeleteReportButton from './deletereportbutton';
// Test Library
import userEvent from '@testing-library/user-event';
import { render, cleanup, screen, waitForElementToBeRemoved } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { TenantProvider } from 'context/TenantContext';
import client from 'utils/apollo';
import store from 'store';
import { ApolloProvider } from '@apollo/client';
import { Provider } from 'react-redux';
import { ErrorBoundary } from 'react-error-boundary';
import ErrorFallback from 'error/ErrorFallback';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import { setupServer } from 'msw/node';
import { handlers } from 'utils/test/handlers';

const graphqlServer = setupServer(...handlers);

afterAll(() => graphqlServer.close());
afterEach(cleanup);
beforeAll(() => graphqlServer.listen());

const Providers = ({ children }) => (
  <IntlProvider locale='en' messages={{}} defaultLocale='en'>
    <TenantProvider>
      <ErrorBoundary FallbackComponent={ErrorFallback}>
        <Provider store={store}>
          <ApolloProvider client={client}>{children}</ApolloProvider>
        </Provider>
      </ErrorBoundary>
    </TenantProvider>
  </IntlProvider>
);
const props = {
  refresh: () => {},
  selectedRows: [],
};

test('DeleteButton is in the DOM', () => {
  render(<DeleteReportButton {...props} />, {
    wrapper: Providers,
  });
  expect(screen.getByTestId('DeleteReportButtonTestId')).toBeInTheDocument();
});

test('DeleteButton open dialog', () => {});
