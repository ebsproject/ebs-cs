import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS AND ATOMS TO USE
import { IconButton, Menu, Tooltip } from '@material-ui/core';
import { PostAdd } from '@material-ui/icons';
import AddProductButton from 'components/molecule/AddProductButton';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  paper: {
    marginRight: theme.spacing(2),
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const NewDomainsMenuButtonMolecule = React.forwardRef(({ rowSelected, refresh }, ref) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const classes = useStyles();

  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    /* 
     @prop data-testid: Id to use inside newdomainsmenubutton.test.js file.
     */
    <div
      ref={ref}
      data-testid={'NewDomainsMenuButtonTestId'}
      className={classes.root}
    >
      <Tooltip title={<FormattedMessage id='none' defaultMessage='New' />}>
        <IconButton
          aria-controls='new-domain-menu'
          aria-haspopup='true'
          color='inherit'
          onClick={handleOpen}
        >
          <PostAdd />
        </IconButton>
      </Tooltip>
      <Menu
        id='new-domain-menu'
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
      >
        <AddProductButton rowSelected={rowSelected} refresh={refresh} handleMenuClose={handleMenuClose} />
      </Menu>
    </div>
  );
});
// Type and required properties
NewDomainsMenuButtonMolecule.propTypes = {
  refresh: PropTypes.func.isRequired,
  rowSelected: PropTypes.object,
};
// Default properties
NewDomainsMenuButtonMolecule.defaultProps = {};

export default NewDomainsMenuButtonMolecule;
