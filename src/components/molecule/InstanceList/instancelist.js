import React from 'react'
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import EbsGrid from "ebs-grid-lib"; 
import { useQuery } from "@apollo/client";
import { FIND_TENANT } from "utils/apollo/gql/tenant";
import DomainInstances from "components/molecule/DomainInstances"

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const InstanceListMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const {tenantId} = props

  const { loading, error, data } = useQuery(FIND_TENANT, {
    variables: { id: tenantId },
  });

  if (loading) return null;
  if (error) return `Error! ${error}`;

  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    { Header: "Identifier", accessor: "name", filter: true },   
    { Header: "HOST", accessor: "server", filter: true }, 
    { Header: "PORT", accessor: "port", filter: true }, 
    { Header: "Notes", accessor: "notes", filter: true },       

  ];

const instances = data.findTenant.instances;

const DomainDetail = (rowData) => {    
  return <DomainInstances instanceId={rowData.original.id}/>

 };
  return (
    <EbsGrid
    toolbar={false}
    columns={columns} 
    title={"Instances"}
    data={instances}
    detailcomponent={DomainDetail}
    />
  );
})
// Type and required properties
InstanceListMolecule.propTypes = {
 tenantId: PropTypes.number.isRequired
}
// Default properties
InstanceListMolecule.defaultProps = {
  tenantId: 0
}

export default InstanceListMolecule
