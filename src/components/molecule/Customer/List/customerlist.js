import React from 'react'

import EbsGrid from "ebs-grid-lib";
import ToolbarActionDefault from "components/molecule/ToolbarActions"
import FormCustomer from "../Form"
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const CustomerListMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { ...rest } = props
  const [openAdd, setOpenAdd] = React.useState(false);

  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    { Header: "Name", accessor: "name", filter: true },
    { Header: "Contact Email", accessor: "officialEmail", filter: true },
    { Header: "Alternate Email", accessor: "alternateEmail", filter: true },
    { Header: "Title", accessor: "jobTitle", filter: true },
    { Header: "Logo", accessor: "logo", filter: true },

  ];

  const toolbarActions = (selection, refresh) => {
    const handleRefresh = e => {
      refresh()
    }
    const handlePrint = e => {
      alert('print')
    }
    const handleAdd = e => {
      setOpenAdd(true)
    }
    return (
      <ToolbarActionDefault handlePrint={handlePrint} handleRefresh={handleRefresh} handleAdd={handleAdd} />
    );
  };


  const handleClose = () => {
    setOpenAdd(false);
  };
  return (
    <>
    <EbsGrid
      toolbar={true}
      columns={columns}
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      entity="Customer"
      title={"Customers"}
      toolbaractions={toolbarActions}
      select="multi"
      callstandard="graphql"
    />
     <FormCustomer open={openAdd} handleClose={handleClose}/>
</>
  );
})
// Type and required properties
CustomerListMolecule.propTypes = {

}
// Default properties
CustomerListMolecule.defaultProps = {

}

export default CustomerListMolecule
