import React from 'react'
import PropTypes from 'prop-types'
import { useForm, Form } from 'components/atoms/UserForm'
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useQuery, useMutation } from "@apollo/client";
import { QUERY_ROWS, CREATE_CUSTOMER } from "utils/apollo/gql/tenant";
import { useDispatch, useSelector } from 'react-redux';
import { hideMessage, showMessage } from 'store/ducks/message';
// CORE COMPONENTS AND ATOMS TO USE
import {
  Grid, Select, FormControl, InputLabel, Button,
  List, ListItem, ListItemAvatar, ListItemIcon, ListItemSecondaryAction,
  ListItemText, Avatar, Typography, Icon, Switch, Divider, MenuItem, TextField, Input
} from '@material-ui/core'
import MaskedInput from 'react-text-mask';
import { DropzoneAreaBase, DropzoneArea } from 'material-ui-dropzone';


const useStyles = makeStyles(theme => createStyles({
  previewChip: {
    minWidth: 160,
    maxWidth: 210
  },
}));

function TextMaskCustom(props) {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
      placeholderChar={'\u2000'}
      showMask
    />
  );
}
const initialFValues = {
  id: 0,
  name: "",
  phone: "",
  officialEmail: "",
  jobTitle: "",
  logo: "",
  organization: {
    id: 1
  }
}

const FormMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { open, handleClose, children, ...rest } = props
  const dispatch = useDispatch();
  const classes = useStyles();
  const {
    values,
    setValues,
    errors,
    setErrors,
    handleInputChange,
    resetForm
  } = useForm(initialFValues, true, true);

  const [createCustomer, { data }] = useMutation(CREATE_CUSTOMER);

  const { loading: loadingOrg, error: errorOrg, data: OrgsData } = useQuery(QUERY_ROWS("Organization", "content { id name}"), {
    variables: { size: 100, number: 1 },
  });

  if (loadingOrg) return null;
  if (errorOrg) return `Error! ${errorOrg}`;


  //events
  const handleSubmit = e => {
    e.preventDefault()
 
    createCustomer({ variables: { type: values } }).then(r => {
      dispatch(
        showMessage({
          message: `The customer ${values.name} was added successfully`,
          variant: 'success',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right'
          }
        })       
      )
      resetForm()
      handleClose()
    }).catch(err => {
      dispatch(
        showMessage({
          message: `${err}`,
          variant: 'error',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right'
          }
        })
      )
    })

  }



  return (
    <Form open={open} onSubmit={handleSubmit} handleClose={handleClose} title={"New Customer"} description={"Register a new Customer"}>
      <Grid container>
        <Grid item xs={6}>
        <FormControl>
            <InputLabel htmlFor="name_field">Customer Name</InputLabel>
            <Input

              id="name_field"
              name="name" value={values.NAME} onChange={handleInputChange}
  
            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="phone_field">Phone</InputLabel>
            <Input

              id="phone_field"
              name="phone" value={values.phone} onChange={handleInputChange}
              inputComponent={TextMaskCustom}
            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="email_field">Official Email</InputLabel>
            <Input

              name="officialEmail" value={values.officialEmail} onChange={handleInputChange}
              id="email_field"

            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="job_field">Job</InputLabel>
            <Input

              name="jobTitle" value={values.jobTitle} onChange={handleInputChange}
              id="job_field"

            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="org-select">Organization</InputLabel>
            <Select name="organizationId" labelId="org-label" id="org-select" name="organizationId" value={values.organization.id} onChange={(e) => {
              const { name, value } = e.target
              setValues(state => ({
                ...state,
                organization: {
                  id: value
                }
              }))
            }}  >
              {OrgsData.findOrganizationList.content.map((org) => (
                <MenuItem key={org.id} value={org.id} >
                  {org.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

        </Grid>
        <Grid item xs={6}>

          <DropzoneArea
            acceptedFiles={['image/*']}
            dropzoneText={"Drag and drop an logo here or click"}
            filesLimit={5}
            onChange={(files) => {
              if(files[0]){
                setValues(state => ({
                  ...state,
                  logo: files[0].name
                }))
              }
         
            }}
            onAlert={(message, variant) => console.log(`${variant}: ${message}`)}
            showPreviews={true}
            showPreviewsInDropzone={false}
            useChipsForPreview
            previewGridProps={{ container: { spacing: 1, direction: 'row' } }}
            previewChipProps={{ classes: { root: classes.previewChip } }}
            previewText="Selected files"
          />
        </Grid>

      </Grid>
    </Form>
  )
})
// Type and required properties
FormMolecule.propTypes = {
  handleClose: PropTypes.object,
  open: PropTypes.bool,
  children: PropTypes.node,
}
// Default properties
FormMolecule.defaultProps = {
  handleClose: null,
  open: false,
  children: null,
}

export default FormMolecule
