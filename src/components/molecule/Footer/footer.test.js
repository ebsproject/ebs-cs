import React from "react";
// Component to be Test
import Footer from "./footer";
// Test Library
import { render, cleanup } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);
// Props to send component to be rendered
const props = {};
test("Render correctly", () => {
  const { getByTestId } = render(<Footer {...props}></Footer>);
  expect(getByTestId("FooterTestId")).toBeInTheDocument();
});
