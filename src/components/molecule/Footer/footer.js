import React, { useContext, useState, useEffect } from 'react';
// CORE COMPONENTS AND ATOMS TO USE
import { LinearProgress } from '@material-ui/core';
import { useQuery } from '@apollo/client';
import { FIND_TENANT } from 'utils/apollo/gql/tenant';
import { TenantContext } from 'context/TenantContext';

const FooterMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const [tenant, setTenant] = useState(null);
  const tenantContext = useContext(TenantContext);
  const { tenantSelectedById, intanceSelectedById } = tenantContext.tenantState;

  const { loading, error, data } = useQuery(FIND_TENANT, {
    variables: { id: tenantSelectedById },
  });

  useEffect(() => {
    data && !tenant && setTenant(data.findTenant);
  }, [data]);

  if (loading) {
    return <LinearProgress />;
  } else {
    return (
      <div data-testid={'FooterTestId'}>
        {/* <Typography variant='subtitle1' color='textPrimary'>
          Copyright@2021 Suscription for Customer: {tenant.customer.name},
          Organization: {tenant.organization.name}, Session: {tenant.name}, Instance:{' '}
          {tenant.instances.find((f) => f.id === intanceSelectedById).name}
        </Typography> */}
      </div>
    );
  }
});

// Type and required properties
FooterMolecule.propTypes = {};
// Default properties
FooterMolecule.defaultProps = {};

export default FooterMolecule;
