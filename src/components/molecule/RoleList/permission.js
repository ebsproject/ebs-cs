import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import TreeView from "@material-ui/lab/TreeView";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Send from "@material-ui/icons/Send";
import TreeItem from "@material-ui/lab/TreeItem";
import { transform } from "node-json-transform";
import { useQuery } from "@apollo/client";
import { GET_ROLE_BY_ID } from "utils/apollo/gql/user";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button,
} from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    height: 240,
    flexGrow: 1,
    maxWidth: 400,
  },
});

const RolePermission = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const [fullWidth, setFullWidth] = React.useState(true);
  const [maxWidth, setMaxWidth] = React.useState("sm");
  const { open, handleClose, roleId } = props;
  const { loading, error, data } = useQuery(GET_ROLE_BY_ID, {
    variables: { id: roleId },
  });

  if (loading) return null;
  if (error) return `Error! ${error}`;

  const information = data.findRole;

  var baseMap = {
    item: {
      id: "id",
      name: "name",
      children: "productfunctions",
    },
    operate: [
      {
        run: (val) => "root",
        on: "id",
      },
      {
        run: (val) => `Role ${val}`,
        on: "name",
      },
      {
        run: function (ary) {
          return transform(ary, nestedMap);
        },
        on: "children",
      },
    ],
  };

  var nestedMap = {
    item: {
      id: "product.domain.id",
      name: "product.domain.name",
      children: [{ id: "product.id", name: "product.name", children: [{id: "id", name: "description"}] }],
    },
    operate: [
        {
          run: (val) => `Domain - ${val}`,
          on: "id",
        },
        {
            run: (val) => `Domain - ${val}`,
            on: "name",
          }, 
          {
            run: (val) => `Product - ${val}`,
            on: "children.0.id",
          }, 
          {
            run: (val) => `Product - ${val}`,
            on: "children.0.name",
          },  
          {
            run: (val) => `Permission - ${val}`,
            on: "children.0.children.0.name",
          },   
      ],
  };
  

  var result = transform(information, baseMap);
  console.log(result);
  const renderTree = (nodes) => (
    <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name}>
          
          {nodes.children && Array.isArray(nodes.children)   ? nodes.children.map((node) => renderTree(node)) : null}
    </TreeItem>
  );

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="permissionDialog"
      open={open}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
    >
      <DialogTitle id="permissionDialog">Role - Permission</DialogTitle>
      <DialogContent>
        <TreeView
          className={classes.root}
          defaultCollapseIcon={<ExpandMoreIcon />}
          defaultExpandIcon={<ChevronRightIcon />}
          defaultExpanded={["root"]}
        >
          {renderTree(result)}
        </TreeView>
      </DialogContent>
      <DialogActions>
        <Button
          autoFocus
          onClick={handleClose}
          color="primary"
          startIcon={<Send />}
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
});

RolePermission.propTypes = {
  open: PropTypes.bool.isRequired,
  roleId: PropTypes.number.isRequired,
  handleClose: PropTypes.func,
};

export default RolePermission;
