import React from 'react';
// CORE COMPONENTS AND ATOMS TO USE
import {IconButton, Tooltip, FormGroup} from '@material-ui/core';
import EbsGrid from 'ebs-grid-lib';
import {AssignmentInd} from '@material-ui/icons';
import Permission from './permission';

const RoleListMolecule = React.forwardRef((props, ref) => {
  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState(null);

  const columns = [
    {Header: 'id', accessor: 'id', hidden: true},
    {Header: 'Role', accessor: 'name', filter: true},
    {Header: 'AD Security Group', accessor: 'securityGroup', filter: true},
  ];

  //events
  const handleClickOpen = (data) => {
    setSelectedValue(data.id);
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
  };

  const rowActions = (rowData, refresh) => {
    return (
      <FormGroup row>
        <Tooltip title="View Permissions">
          <IconButton
            aria-label="permission"
            color="primary"
            onClick={() => handleClickOpen(rowData)}
          >
            <AssignmentInd />
          </IconButton>
        </Tooltip>
      </FormGroup>
    );
  };

  return (
    <>
      <EbsGrid
        toolbar={false}
        columns={columns}
        uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
        entity="Role"
        title={'Roles'}
        callstandard="graphql"
        rowactions={rowActions}
      />
      {selectedValue && (
        <Permission
          roleId={selectedValue}
          open={open}
          handleClose={handleClose}
        />
      )}
    </>
  );
});
// Type and required properties
RoleListMolecule.propTypes = {};
// Default properties
RoleListMolecule.defaultProps = {};

export default RoleListMolecule;
