import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import EbsGrid from "ebs-grid-lib"; 
import ToolbarActionDefault from "components/molecule/ToolbarActions"
import FormOrg from "../Form"
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const OrganizationListMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { label, children, ...rest } = props
  const [openAdd, setOpenAdd] = React.useState(false);
  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    { Header: "Organization", accessor: "name", filter: true },   
    { Header: "Legal Name", accessor: "legalName", filter: true }, 
    { Header: "Slogan", accessor: "slogan", filter: true }, 
    { Header: "WebPage", accessor: "webPage", filter: true },   
    { Header: "Logo", accessor: "logo", filter: true },   
    { Header: "Active", accessor: "isActive", filter: true },     

  ];

  const toolbarActions = (selection, refresh) => {
    const handlePrint = e => {
      alert('print')
    }
    const handleAdd = e => {
      setOpenAdd(true)
    }
    return (
      <ToolbarActionDefault handlePrint={handlePrint} handleAdd={handleAdd} />
    );
  };
  const handleClose = () => {
    setOpenAdd(false);
  };

  return (
    <>
    <EbsGrid
    toolbar={true}
    columns={columns}
    uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
    entity="Organization"
    title={"Organizations"}
    callstandard="graphql"
    toolbaractions={toolbarActions}
    select="multi"
    />
     <FormOrg open={openAdd} handleClose={handleClose}/>
    </>
  );
})
// Type and required properties
OrganizationListMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}
// Default properties
// OrganizationListMolecule.defaultProps = {
//   label: 'Hello world',
//   children: null,
// }

export default OrganizationListMolecule
