import React from 'react'
import PropTypes from 'prop-types'
import { useForm, Form } from 'components/atoms/UserForm'
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useQuery, useMutation } from "@apollo/client";
import { QUERY_ROWS, CREATE_ORGANIZATION } from "utils/apollo/gql/tenant";
import { useDispatch, useSelector } from 'react-redux';
import { hideMessage, showMessage } from 'store/ducks/message';
import {
  Grid, Select, FormControl, InputLabel, Button,
  List, ListItem, ListItemAvatar, ListItemIcon, ListItemSecondaryAction,
  ListItemText, Avatar, Typography, Icon, Switch, Divider, MenuItem, TextField, Input
} from '@material-ui/core'
import MaskedInput from 'react-text-mask';
import { DropzoneAreaBase, DropzoneArea } from 'material-ui-dropzone';




const useStyles = makeStyles(theme => createStyles({
  previewChip: {
    minWidth: 160,
    maxWidth: 210
  },
}));

function TextMaskCustom(props) {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
      placeholderChar={'\u2000'}
      showMask
    />
  );
}

const initialFValues = {
  id: 0,
  name: "",
  phone: "",
  code: "",
  legalName: "",
  logo: "",
  slogan: "",
  customer: {
    id: 1
  }
}

const FormMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { open, handleClose, children, ...rest } = props
  const dispatch = useDispatch();
  const classes = useStyles();
  const {
    values,
    setValues,
    errors,
    setErrors,
    handleInputChange,
    resetForm
  } = useForm(initialFValues, true, true);

  const [createOrganization, { data }] = useMutation(CREATE_ORGANIZATION);

  const { loading: loadingCust, error: errorCust, data: CustData } = useQuery(QUERY_ROWS("Customer", "content { id name}"), {
    variables: { size: 100, number: 1 },
  });

  if (loadingCust) return null;
  if (errorCust) return `Error! ${errorCust}`;

  const handleSubmit = e => {
    e.preventDefault()
     createOrganization({ variables: { type: values } }).then(r => {
      dispatch(
        showMessage({
          message: `The Organization ${values.name} was added successfully`,
          variant: 'success',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right'
          }
        })       
      )
      resetForm()
      handleClose()
    }).catch(err => {
      dispatch(
        showMessage({
          message: `${err}`,
          variant: 'error',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right'
          }
        })
      )
    })
  }

  return (
    <Form open={open} onSubmit={handleSubmit} handleClose={handleClose} title={"New Organization"} description={"Register a new Organization"}>
      <Grid container>
        <Grid item xs={6}>
          <TextField label="Organization Name" name="name" value={values.name} onChange={handleInputChange}></TextField>
          
     
          <FormControl>
            <InputLabel htmlFor="legal_field">Legal Name</InputLabel>
            <Input

              name="legalName" value={values.legalName} onChange={handleInputChange}
              id="legal_field"

            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="slogan_field">Slogan</InputLabel>
            <Input

              name="slogan" value={values.slogan} onChange={handleInputChange}
              id="slogan_field"

            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="phone_field">Phone</InputLabel>
            <Input

              id="phone_field"
              name="phone" value={values.phone} onChange={handleInputChange}
              inputComponent={TextMaskCustom}
            />
          </FormControl>
          <FormControl>
            <InputLabel htmlFor="org-select">Customer</InputLabel>
            <Select name="customerId" labelId="cust-label" id="cust-select" name="customerId" value={values.customer.id} onChange={(e) => {
              const { name, value } = e.target
              setValues(state => ({
                ...state,
                customer: {
                  id: value
                }
              }))
            }}  >
              {CustData.findCustomerList.content.map((cust) => (
                <MenuItem key={cust.id} value={cust.id} >
                  {cust.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

        </Grid>
        <Grid item xs={6}>

          <DropzoneArea
            acceptedFiles={['image/*']}
            dropzoneText={"Drag and drop an logo here or click"}
            filesLimit={5}
            onChange={(files) => {
              if(files[0]){
                setValues(state => ({
                  ...state,
                  logo: files[0].name
                }))
              }
         
            }}
            onAlert={(message, variant) => console.log(`${variant}: ${message}`)}
            showPreviews={true}
            showPreviewsInDropzone={false}
            useChipsForPreview
            previewGridProps={{ container: { spacing: 1, direction: 'row' } }}
            previewChipProps={{ classes: { root: classes.previewChip } }}
            previewText="Selected files"
          />
        </Grid>

      </Grid>
      </Form>
  )
})
// Type and required properties
FormMolecule.propTypes = {
  handleClose: PropTypes.object,
  open: PropTypes.bool,
  children: PropTypes.node,
}
// Default properties
FormMolecule.defaultProps = {
  handleClose: null,
  open: false,
  children: null,
}

export default FormMolecule
