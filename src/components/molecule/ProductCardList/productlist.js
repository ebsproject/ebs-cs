import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { TenantContext } from 'context/TenantContext';
import { InstanceContext } from 'context/InstanceContext';
import {
  Button,
  Grid,
  Typography,
  Card,
  CardContent,
  CardActions,
  CardMedia,
} from '@material-ui/core';
import { useQuery } from '@apollo/client';
import { FIND_INSTANCE } from 'utils/apollo/gql/tenant';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    width: 250,
    height: 275,
  },
  CardHeader: {
    background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  media: {
    maxWidth: 250,
    width: 250,
    maxHeight: 155,
    height: 155,
  },
}));

const ProductListMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const classes = useStyles();
  const tenantContext = useContext(TenantContext);
  const instanceContext = useContext(InstanceContext);
  const { tenantSelectedById, intanceSelectedById } = tenantContext.tenantState;
  const history = useHistory();

  const { loading, error, data } = useQuery(FIND_INSTANCE, {
    variables: { id: intanceSelectedById },
  });

  if (error) throw error;
  if (loading) return null;

  const domains = data.findInstance.domaininstances;

  //events
  const handleClickDomain = (item) => {
    try {
      const { hostname, port, protocol } = new URL(item.context);
      if (hostname === window.location.hostname && item.mfe) {
        tenantContext.setTenantState({
          tenantId: Number(tenantSelectedById),
          instanceId: Number(intanceSelectedById),
          domainId: Number(item.domain.id),
        });
        instanceContext.setInstanceState(domains);
        const trfDomain = item.domain.name
          .split(' ')
          .map((word) => word.toLowerCase().charAt(0))
          .join('');

        history.push(`/${trfDomain}`);
      } else {
        window.open(item.context, '__blank');
      }
    } catch {
      alert("The component don't have url");
    }
  };

  return (
    <div className='flex flex-col flex-auto flex-shrink-0 items-center justify-center p-32'>
      <Grid
        container
        direction='row'
        justify='flex-start'
        alignItems='flex-start'
        data-testid={'TenantListTestId'}
        spacing={4}
      >
        <Grid item xs={12} className={classes.CardHeader}>
          <Typography variant='h6'>
            <FormattedMessage
              id='tnt.comp.comsoon.welcome'
              defaultMessage={`EBS Services for ${data.findInstance.name} Instance`}
            />
          </Typography>
        </Grid>
        {domains
          .slice()
          .sort(function (a, b) {
            return a.id - b.id;
          })
          .map((domain) => (
            <Grid key={domain.id} item xs={6}>
              <Card key={domain.id} className={classes.root}>
                <CardMedia
                  className={classes.media}
                  component='img'
                  image={`assets/images/domains/${domain.domain.icon}`}
                  title={domain.domain.name}
                />

                <CardContent>
                  <Typography variant='h5' component='h2'>
                    {domain.domain.name}
                  </Typography>
                  <Typography variant='body2' color='textSecondary' component='p'>
                    {domain.domain.info}
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button
                    size='small'
                    color='primary'
                    onClick={(event) => handleClickDomain(domain)}
                  >
                    <FormattedMessage
                      id='tnt.tenant.select'
                      defaultMessage='Go to'
                    />
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
      </Grid>
    </div>
  );
});

export default ProductListMolecule;
