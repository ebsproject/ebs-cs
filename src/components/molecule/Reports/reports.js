import React from 'react';
import PropTypes from 'prop-types';
import EbsGrid from 'ebs-grid-lib';
import { Box } from '@material-ui/core';
import ApolloClient from 'utils/apollo';
import NewReportsMenuButton from 'components/molecule/NewReportsMenu';
import DeleteReportButton from 'components/molecule/DeleteReportButton';
import ReportDesignerButton from 'components/molecule/ReportDesignerButton';
import { useQuery } from '@apollo/client';
import { FIND_PRINTOUT_TEMPLATE_LIST } from 'utils/apollo/gql/printoutManager';
import ReportsDetail from 'components/molecule/ReportsDetail';
import { useDispatch } from 'react-redux';
import ModifyReportButton from 'components/molecule/ModifyReportButton';
import { showMessage } from 'store/ducks/message';

// import PermissionButton from "components/molecule/PermissionButton";

const ReportsMolecule = React.forwardRef((props, ref) => {
  const [reportList, setReportList] = React.useState(null);
  const [page, setPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const dispatch = useDispatch();

  const columns = [
    { Header: 'id', accessor: 'id', hidden: true },
    { Header: 'Name', accessor: 'name', filter: true },
    { Header: 'Description', accessor: 'description', filter: true },
    { Header: 'ZPL', accessor: 'zpl', filter: true },
  ];
  const { loading, error, data } = useQuery(FIND_PRINTOUT_TEMPLATE_LIST, {
    variables: {
      page: { number: page, size: 10 },
      sort: { col: 'id', mod: 'DES' },
    },
    fetchPolicy: 'no-cache',
  });
  error &&
    dispatch(
      showMessage({
        message: 'Error trying to get template list',
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
      }),
    );

  React.useEffect(() => {
    if (data) {
      setTotalPages(data.findPrintoutTemplateList.totalPages);
      setReportList(data.findPrintoutTemplateList.content);
    }
  }, [data]);

  const fetch = (page, pageSize, columnsToFilter, value) => {
    let filters = [];
    columnsToFilter.length > 0 &&
      columnsToFilter.map((filter) =>
        filters.push({ mod: 'LK', col: filter, val: value }),
      );

    columnsToFilter.length === 0 &&
      value &&
      columns.map((column) => {
        column.filter &&
          filters.push({ mod: 'LK', col: column.accessor, val: value });
      });

    ApolloClient.query({
      query: FIND_PRINTOUT_TEMPLATE_LIST,
      variables: {
        page: { number: page, size: pageSize },
        filters: filters,
        sort: { col: 'id', mod: 'DES' },
      },
      fetchPolicy: 'no-cache',
    }).then(({ data }) => {
      setPage(page);
      setTotalPages(data.findPrintoutTemplateList.totalPages);
      setReportList(data.findPrintoutTemplateList.content);
    });
  };

  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box display='flex' flexDirection='row'>
        <Box>
          <NewReportsMenuButton refresh={refresh} />
        </Box>
        <Box>
          <DeleteReportButton selectedRows={selectedRows} refresh={refresh} />
        </Box>
      </Box>
    );
  };

  const rowActions = (rowData, refresh) => {
    return (
      <Box display='flex' flexDirection='row'>
        <Box>
          <ReportDesignerButton rowData={rowData} refresh={refresh} />
        </Box>
        {/* <Box>
          <PermissionButton rowData={rowData} />
        </Box> */}
        <Box>
          <ModifyReportButton rowData={rowData} refresh={refresh} />
        </Box>
      </Box>
    );
  };

  const ReportDetail = (rowData) => {
    const { original } = rowData;
    return (
      <ReportsDetail
        productsData={original.products}
        programsData={original.programs}
      />
    );
  };

  return (
    <div data-testid={'ReportsTestId'} ref={ref}>
      <EbsGrid
        toolbar={true}
        toolbaractions={toolbarActions}
        title={'Reports'}
        columns={columns}
        data={reportList}
        detailcomponent={ReportDetail}
        fetch={fetch}
        page={page}
        totalPages={totalPages}
        rowactions={rowActions}
        select='multi'
        pagination
      />
    </div>
  );
});
// Type and required properties
ReportsMolecule.propTypes = {};
// Default properties
ReportsMolecule.defaultProps = {};

export default ReportsMolecule;
