import React from 'react';
import PropTypes from 'prop-types';
import EbsGrid from 'ebs-grid-lib';
import { FIND_PRODUCT_LIST } from 'utils/apollo/gql/tenantManagement';
import ApolloClient from 'utils/apollo';
import { Box } from '@material-ui/core';
import ModifyProductButton from 'components/molecule/ModifyProductButton';

const ProductListMolecule = React.forwardRef(({ domainData }, ref) => {
  const [products, setProducts] = React.useState(null);

  async function fetch(page, pageSize, columnsToFilter, value) {
    const { data, error } = await ApolloClient.query({
      query: FIND_PRODUCT_LIST,
      variables: {
        page: { number: 1, size: 100 },
        filters: [{ mod: 'EQ', col: 'domain.id', val: domainData.id }],
      },
      fetchPolicy: 'no-cache',
    });
    let newProductList = null;
    newProductList = data.findProductList.content
      .slice()
      .sort((a, b) => a.menuOrder - b.menuOrder);
    setProducts(newProductList);
  }

  const columns = [
    { Header: 'id', accessor: 'id', hidden: true },
    { Header: 'Product', accessor: 'name', filter: true },
    { Header: 'Menu Order', accessor: 'menuOrder', filter: true },
    { Header: 'Path', accessor: 'path', hidden: true },
    { Header: 'Description', accessor: 'description', hidden: true },
    { Header: 'Help', accessor: 'help', hidden: true },
  ];

  const rowActions = (rowData, refresh) => {
    return (
      <Box display='flex'>
        <Box>
          <ModifyProductButton
            rowData={rowData}
            refresh={refresh}
            domainData={domainData}
          />
        </Box>
      </Box>
    );
  };

  return (
    <div data-testid='ProductListTestId'>
      <EbsGrid
        columns={columns}
        data={products}
        fetch={fetch}
        toolbar={false}
        rowactions={rowActions}
      />
    </div>
  );
});
// Type and required properties
ProductListMolecule.propTypes = {
  domainData: PropTypes.object.isRequired,
};
// Default properties
ProductListMolecule.defaultProps = {};

export default ProductListMolecule;
