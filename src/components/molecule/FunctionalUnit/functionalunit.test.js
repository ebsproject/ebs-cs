import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import FunctionalUnit from './functionalunit'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FunctionalUnit></FunctionalUnit>, div)
})
// Props to send component to be rendered
const props = {
  properyName: 'Value',
}
test('Render correctly', () => {
  const { getByTestId } = render(<FunctionalUnit {...props}></FunctionalUnit>)
  expect(getByTestId('FunctionalUnitTestId')).toBeInTheDocument()
})
