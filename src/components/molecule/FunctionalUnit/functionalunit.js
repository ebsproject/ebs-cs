import React from 'react';
// CORE COMPONENTS AND ATOMS TO USE
import EbsGrid from 'ebs-grid-lib';

import {transform} from 'node-json-transform';
import {useQuery} from '@apollo/client';
import {QUERY_ROWS} from 'utils/apollo/gql/tenant';
//MAIN FUNCTION

const FunctionalUnitMolecule = React.forwardRef((props, ref) => {
  const {loading, error, data} = useQuery(
    QUERY_ROWS(
      'FunctionalUnit',
      'content { id name type notes  program { id name crop { id name } } }'
    ),
    {
      variables: {size: 100, number: 1},
    }
  );

  if (loading) return null;
  if (error) return `Error! ${error}`;

  const map = {
    item: {
      id: 'id',
      name: 'name',
      type: 'type',
      notes: 'notes',
      program: 'program.name',
      crop: 'program.crop.name',
    },
  };

  const result = transform(data.findFunctionalUnitList.content, map);

  const columns = [
    {Header: 'id', accessor: 'id', hidden: true},
    {Header: 'Functional Unit', accessor: 'name', filter: true},
    {Header: 'Type', accessor: 'type', filter: true},
    {Header: 'Program', accessor: 'program', filter: true},
    {Header: 'Crop', accessor: 'crop', filter: true},
    {Header: 'Notes', accessor: 'notes', filter: true},
  ];

  return (
    <EbsGrid
      toolbar={false}
      columns={columns}
      data={result}
      title={'Functional Unit'}
    />
  );
});
// Type and required properties
FunctionalUnitMolecule.propTypes = {};
// Default properties
FunctionalUnitMolecule.defaultProps = {};

export default FunctionalUnitMolecule;
