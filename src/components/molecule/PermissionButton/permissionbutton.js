import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND ATOMS TO USE
import {
  AppBar,
  Dialog,
  DialogContent,
  IconButton,
  Slide,
  Toolbar,
  Tooltip,
  Typography,
} from "@material-ui/core";
import { Close, ViewColumn } from "@material-ui/icons";
import ViewZpl from "components/atoms/ZplViewer";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PermissionButtonMolecule = React.forwardRef(({ rowData }, ref) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    /* 
     @prop data-testid: Id to use inside permissionbutton.test.js file.
     */
    <div ref={ref} data-testid={"PermissionButtonTestId"}>
      <Tooltip
        title={<FormattedMessage id="none" defaultMessage="Permission" />}
      >
        <IconButton
          aria-label="permission"
          color="primary"
          onClick={handleOpen}
        >
          <ViewColumn />
        </IconButton>
      </Tooltip>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <Close />
            </IconButton>
            <Typography variant="h6">
              <FormattedMessage id="none" defaultMessage="View ZPL" />
            </Typography>
          </Toolbar>
        </AppBar>
        <DialogContent>
          <ViewZpl />
        </DialogContent>
      </Dialog>
    </div>
  );
});
// Type and required properties
PermissionButtonMolecule.propTypes = {
  rowData: PropTypes.object,
};
// Default properties
PermissionButtonMolecule.defaultProps = {};

export default PermissionButtonMolecule;
