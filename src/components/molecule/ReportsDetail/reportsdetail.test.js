import React from 'react'
// Component to be Test
import ReportsDetail from './reportsdetail'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  productsData: [],
  programsData: []
}

test('Render correctly', () => {
  const { getByTestId } = render(<ReportsDetail {...props}></ReportsDetail>)
  expect(getByTestId('ReportsDetailTestId')).toBeInTheDocument()
})
