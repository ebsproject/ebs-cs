import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { AppBar, Box, Tab, Tabs } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ProductsLis from "components/atoms/PrintoutProductsGrid";
import ProgramsLis from "components/atoms/PrintoutProgramsGrid";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: "100%",
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ReportsDetailMolecule = React.forwardRef((props, ref) => {
  const { productsData, programsData } = props;
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    /* 
     @prop data-testid: Id to use inside reportsdetail.test.js file.
     */
    <div ref={ref} data-testid={"ReportsDetailTestId"} className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Products" {...a11yProps(0)} />
          <Tab label="Programs" {...a11yProps(1)} />
        </Tabs>
        <TabPanel value={value} index={0}>
          <ProductsLis data={productsData} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <ProgramsLis data={programsData} />
        </TabPanel>
      </AppBar>
    </div>
  );
});
// Type and required properties
ReportsDetailMolecule.propTypes = {
  productsData: PropTypes.array.isRequired,
  programsData: PropTypes.array.isRequired
};
// Default properties
ReportsDetailMolecule.defaultProps = {};

export default ReportsDetailMolecule;
