import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS AND ATOMS TO USE
import {
  IconButton,
  Menu,
  Tooltip,
} from '@material-ui/core';
import { PostAdd } from '@material-ui/icons';
import AddReportButton from 'components/molecule/AddReportButton';
import NewReportBasedOnButton from 'components/molecule/NewReportBasedOnButton';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  paper: {
    marginRight: theme.spacing(2),
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const NewReportsMenuMolecule = React.forwardRef(({ refresh }, ref) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const classes = useStyles();

  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    /* 
     @prop data-testid: Id to use inside newreportsmenu.test.js file.
     */
    <div ref={ref} data-testid={'NewReportsMenuTestId'} className={classes.root}>
      <Tooltip title={<FormattedMessage id='none' defaultMessage='New report' />}>
        <IconButton
          aria-controls='new-report-menu'
          aria-haspopup='true'
          color='inherit'
          onClick={handleOpen}
        >
          <PostAdd />
        </IconButton>
      </Tooltip>
      <Menu
        id='new-report-menu'
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
      >
        <AddReportButton refresh={refresh} handleMenuClose={handleMenuClose} />
        <NewReportBasedOnButton
          refresh={refresh}
          handleMenuClose={handleMenuClose}
        />
      </Menu>
    </div>
  );
});
// Type and required properties
NewReportsMenuMolecule.propTypes = {
  refresh: PropTypes.func.isRequired,
};
// Default properties
NewReportsMenuMolecule.defaultProps = {};

export default NewReportsMenuMolecule;
