import React from 'react';
import PropTypes from 'prop-types';
import {IconButton} from '@material-ui/core';
import AddIcon from '@material-ui/icons/AddCircle';
import PrintIcon from '@material-ui/icons/Print';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import RefreshIcon from '@material-ui/icons/Refresh';

const ToolbarActionsMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    handleAdd,
    handleEdit,
    handleDelete,
    handlePrint,
    handleRefresh,
    ...rest
  } = props;

  return (
    <>
      <IconButton title={'Add'} onClick={handleAdd} color={'inherit'}>
        <AddIcon />
      </IconButton>
      <IconButton title={'Edit'} onClick={handleEdit} color={'inherit'}>
        <EditIcon />
      </IconButton>
      <IconButton title={'Delete'} onClick={handleDelete} color={'inherit'}>
        <DeleteIcon />
      </IconButton>
      <IconButton title={'Print'} onClick={handlePrint} color={'inherit'}>
        <PrintIcon />
      </IconButton>
      <IconButton title={'Refresh'} onClick={handleRefresh} color={'inherit'}>
        <RefreshIcon />
      </IconButton>
      {children}
    </>
  );
});
// Type and required properties
ToolbarActionsMolecule.propTypes = {
  handlePrint: PropTypes.object,
  handleAdd: PropTypes.object,
  handleRefresh: PropTypes.object,
  children: PropTypes.node,
};
// Default properties
ToolbarActionsMolecule.defaultProps = {
  handlePrint: null,
  handleAdd: null,
  handleRefresh: null,
  children: null,
};

export default ToolbarActionsMolecule;
