import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND MOLECULES TO USE
import { makeStyles } from "@material-ui/core/styles";
import { Tab, Tabs, AppBar, Box } from "@material-ui/core";
import {List as UserList} from "components/molecule/User";
import FunctionalUnit from "components/molecule/FunctionalUnit";
import Roles from "components/molecule/RoleList";
//MAIN FUNCTION

//MAIN FUNCTION
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `tab-${index}`,
    "aria-controls": `tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

const UsersOrganism = React.forwardRef((props, ref) => {
  // Properties of the organism
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="tenant management"
        >
          <Tab label="Users" {...a11yProps(0)} />
          <Tab label="Roles" {...a11yProps(1)} />
          <Tab label="Functional Units" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <UserList/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Roles/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <FunctionalUnit/>
      </TabPanel>
    </div>
  );
});
// Type and required properties
UsersOrganism.propTypes = {};
// Default properties
UsersOrganism.defaultProps = {};

export default UsersOrganism;
