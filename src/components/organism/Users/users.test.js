import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import Users from './users'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
}
test('Render correctly', () => {
  const { getByTestId } = render(<Users {...props}></Users>)
  expect(getByTestId('UsersTestId')).toBeInTheDocument()
})
