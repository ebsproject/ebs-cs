import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
// CORE COMPONENTS AND MOLECULES TO USE
import { Tab, Tabs, Box } from "@material-ui/core";
import {List as Tenants} from "components/molecule/Tenant";
import {List as Organizations} from "components/molecule/Organization";
import {List as Customers} from "components/molecule/Customer";
import {List as Domains} from "components/molecule/Domain";

//MAIN FUNCTION
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `tab-${index}`,
    "aria-controls": `tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: '100%',
  
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    minWidth: '130px',
    marginLeft: '0%'
  },
}));

const TenantOrganism = React.forwardRef((props, ref) => {
  // Properties of the organism
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>

        <Tabs
         orientation='vertical'
         variant='scrollable'
          value={value}
          onChange={handleChange}
          aria-label="tenant management"
          className={classes.tabs}
        >
          <Tab label="Tenants" {...a11yProps(0)} />
          <Tab label="Domains" {...a11yProps(1)} />
          <Tab label="Customers" {...a11yProps(2)} />
          <Tab label="Organizations" {...a11yProps(3)} />
        </Tabs>
     
      <TabPanel value={value} index={0}>
        <Tenants />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Domains />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Customers />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Organizations />
      </TabPanel>
    </div>
  );
});
// Type and required properties
TenantOrganism.propTypes = {};
// Default properties
TenantOrganism.defaultProps = {};

export default TenantOrganism;
