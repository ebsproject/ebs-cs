import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
//core components
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Tooltip,
  Typography,
} from '@material-ui/core';

import client from 'utils/apollo';
import { showMessage } from 'store/ducks/message';
import { QUERY_ROWS, DELETE_TENANT } from 'utils/apollo/gql/tenant';
import { DeleteForever } from '@material-ui/icons';
import { FormattedMessage } from 'react-intl';

//This is the main function
const ToolbarActionDelete = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { selection, refresh, ...rest } = props;
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = async () => {
    if (selection.length) {
      const { original: tenantSelected } = selection[0];

      const result = await client.mutate({
        mutation: DELETE_TENANT,
        variables: { id: Number(tenantSelected.id) },
        fetchPolicy: 'no-cache',
      });

      dispatch(
        showMessage({
          message: `The tenant was deleted successfully`,
          variant: 'success',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
        }),
      );
      handleClose();
      refresh();
    }
  };

  return (
    <div>
      <Tooltip
        title={<FormattedMessage id='none' defaultMessage='Delete product' />}
      >
        <span>
          <IconButton
            disabled={selection.length < 1 ? true : false}
            aria-label='DeleteTenantButton'
            onClick={handleClickOpen}
            color='inherit'
          >
            <DeleteForever />
          </IconButton>
        </span>
      </Tooltip>
      <Dialog fullWidth keepMounted maxWidth='sm' open={open} onClose={handleClose}>
        <DialogTitle>
          <FormattedMessage id='none' defaultMessage='Delete Tenant' />
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <FormattedMessage
              id='none'
              defaultMessage='Are you sure to delete tenant?'
            />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <Typography variant='button' color='secondary'>
              <FormattedMessage id='none' defaultMessage='Cancel' />
            </Typography>
          </Button>
          <Button onClick={handleDelete}>
            <Typography variant='button'>
              <FormattedMessage id='none' defaultMessage='Confirm' />
            </Typography>
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
ToolbarActionDelete.propTypes = {
  selection: PropTypes.array,
  refresh: PropTypes.func,
};
// Default properties
ToolbarActionDelete.defaultProps = {};

export default ToolbarActionDelete;
