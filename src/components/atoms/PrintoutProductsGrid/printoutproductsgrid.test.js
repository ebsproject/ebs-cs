import React from "react";
// Component to be Test
import PrintoutProductsGrid from "./printoutproductsgrid";
// Test Library
import { render, cleanup } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

// Props to send component to be rendered
const props = {
  data: [
    {
      id: 1,
      name: "Example",
      domain: { name: "Domain Example" },
    },
    {
      id: 2,
      name: "Example",
      domain: { name: "Domain Example" },
    },
    {
      id: 3,
      name: "Example",
      domain: { name: "Domain Example" },
    },
  ],
};

test("Render correctly", () => {
  const { getByTestId } = render(
    <PrintoutProductsGrid {...props}></PrintoutProductsGrid>
  );
  expect(getByTestId("PrintoutProductsGridTestId")).toBeInTheDocument();
});
