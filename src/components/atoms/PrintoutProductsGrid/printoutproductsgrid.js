import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import EbsGrid from "ebs-grid-lib";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PrintoutProductsGridAtom = React.forwardRef((props, ref) => {
  const [data, setData] = React.useState(null);
  React.useEffect(() => {
    let newData = [];
    props.data.map((product) => {
      newData.push({
        id: product.id,
        name: product.name,
        domain: product.domain.name,
      });
    });
    setData(newData);
  }, [props.data]);
  const columns = [
    { Header: "Id", accessor: "id", hidden: true },
    { Header: "Name", accessor: "name" },
    { Header: "Domain", accessor: "domain" },
  ];

  return (
    /* 
     @prop data-testid: Id to use inside printoutproductsgrid.test.js file.
     */
    <div data-testid={"PrintoutProductsGridTestId"} ref={ref}>
      <EbsGrid columns={columns} data={data} fetch={() => {}} />
    </div>
  );
});
// Type and required properties
PrintoutProductsGridAtom.propTypes = {
  data: PropTypes.array.isRequired
};
// Default properties
PrintoutProductsGridAtom.defaultProps = {};

export default PrintoutProductsGridAtom;
