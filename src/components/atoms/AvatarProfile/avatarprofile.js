import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import {
  Popover,
  Typography,
  Icon,
  ListItemText,
  Avatar,
  Chip,
  MenuItem,
  Link,
  ListItemIcon,
  Divider,
} from '@material-ui/core';

import { FormattedMessage } from 'react-intl';
import { logoutUser } from 'store/ducks/login';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AvatarProfileAtom = React.forwardRef((props, ref) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [placement, setPlacement] = React.useState();
  const dispatch = useDispatch();
  const classes = useStyles();
  const userInfo = useSelector(({ user }) => user);

  const handleClick = (newPlacement) => (event) => {
    setAnchorEl(event.currentTarget);
    setOpen((prev) => placement !== newPlacement || !prev);
    setPlacement(newPlacement);
  };

  const handleClose = () => {
    setOpen(false);
  };

  async function handleLogin() {}

  const handleLogout = async () => {
    await dispatch(logoutUser());
  };

  if (!userInfo) {
    return null;
  }

  const { person: personInfo } = userInfo;

  return (
    /* 
     @prop data-testid: Id to use inside avatarprofile.test.js file.
     */
    <React.Fragment>
      {userInfo.userName != null ? (
        <Chip
          avatar={<Avatar className='' alt='user photo' src={userInfo.photo} />}
          label={
            <React.Fragment>
              <Typography className={classes.title} variant='subtitle1' noWrap>
                {`${personInfo.familyName}, ${personInfo.givenName}`}
              </Typography>
              <Divider />
              <Typography className={classes.title} variant='subtitle2' noWrap>
                {personInfo.jobTitle}
              </Typography>
            </React.Fragment>
          }
          onClick={handleClick('bottom-end')}
          variant='default'
          color='primary'
          clickable
        />
      ) : (
        <Chip size='small' />
      )}
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <MenuItem component={Link} to='/profile' onClick={handleClose} role='button'>
          <ListItemIcon>
            <Icon>account_circle</Icon>
          </ListItemIcon>
          <ListItemText
            primary={
              <FormattedMessage
                id='tnt.nav.profile.lbl.profile'
                defaultMessage='Profile'
              />
            }
          />
        </MenuItem>
        <MenuItem
          component={Link}
          to='/account/general'
          onClick={handleClose}
          role='button'
        >
          <ListItemIcon>
            <Icon>settings_applications</Icon>
          </ListItemIcon>
          <ListItemText
            primary={
              <FormattedMessage
                id='tnt.nav.profile.lbl.account'
                defaultMessage='Account'
              />
            }
          />
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleLogout();
          }}
        >
          <ListItemIcon>
            <Icon>exit_to_app</Icon>
          </ListItemIcon>
          <ListItemText
            primary={
              <FormattedMessage
                id='tnt.nav.profile.lbl.logout'
                defaultMessage='Logout'
              />
            }
          />
        </MenuItem>
      </Popover>
    </React.Fragment>
  );
});
// Type and required properties
AvatarProfileAtom.propTypes = {};
// Default properties
AvatarProfileAtom.defaultProps = {};

export default AvatarProfileAtom;
