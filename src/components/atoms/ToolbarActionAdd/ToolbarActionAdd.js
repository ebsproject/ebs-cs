import React, { useState } from 'react';
import PropTypes from 'prop-types';
//core components
import { FormattedMessage } from 'react-intl';
import AddIcon from '@material-ui/icons/AddCircle';
import { IconButton, Tooltip } from '@material-ui/core';

import FormTenant from 'components/molecule/Tenant/Form';

//This is the main function
const ToolbarActionAdd = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { refresh, ...rest } = props;

  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  // Style classes
  return (
    <div ref={ref} data-testid={'ToolbarActionAddTestId'}>
      <Tooltip title={<FormattedMessage id='none' defaultMessage='New Tenant' />}>
        <IconButton title={'Add'} onClick={handleOpen} color={'inherit'}>
          <AddIcon />
        </IconButton>
      </Tooltip>
      <FormTenant open={open} refresh={refresh} handleClose={handleClose} />
    </div>
  );
});
// Type and required properties
ToolbarActionAdd.propTypes = {
  refresh: PropTypes.func.isRequired,
};
// Default properties
ToolbarActionAdd.defaultProps = {};

export default ToolbarActionAdd;
