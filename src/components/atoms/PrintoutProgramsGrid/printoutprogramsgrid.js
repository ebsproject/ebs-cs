import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import EbsGrid from "ebs-grid-lib";
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PrintoutProgramsGridAtom = React.forwardRef((props, ref) => {
  const { data } = props;
  const columns = [
    { Header: "Id", accessor: "id", hidden: true },
    { Header: "Name", accessor: "name" },
  ];

  return (
    /* 
     @prop data-testid: Id to use inside printoutprogramsgrid.test.js file.
     */
    <div data-testid={"PrintoutProgramsGridTestId"} ref={ref}>
      <EbsGrid columns={columns} data={data} fetch={() => {}} />
    </div>
  );
});
// Type and required properties
PrintoutProgramsGridAtom.propTypes = {
  data: PropTypes.array.isRequired
};
// Default properties
PrintoutProgramsGridAtom.defaultProps = {};

export default PrintoutProgramsGridAtom;
