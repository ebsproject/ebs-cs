import React from "react";
// Component to be Test
import PrintoutProgramsGrid from "./printoutprogramsgrid";
// Test Library
import { render, cleanup } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

// Props to send component to be rendered
const props = {
  data: [
    {
      id: 1,
      name: "Example",
    },
    {
      id: 2,
      name: "Example",
    },
    {
      id: 3,
      name: "Example",
    },
  ],
};

test("Render correctly", () => {
  const { getByTestId } = render(
    <PrintoutProgramsGrid {...props}></PrintoutProgramsGrid>
  );
  expect(getByTestId("PrintoutProgramsGridTestId")).toBeInTheDocument();
});
