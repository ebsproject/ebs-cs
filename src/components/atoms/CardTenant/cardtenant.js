import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { useQuery } from "@apollo/client";
import { FIND_TENANT } from "utils/apollo/gql/tenant";

import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  CardMedia,
  Grid,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    width: 250,
    height: 350,
  },

  media: {
    maxWidth: 250,
    width: "250",
    maxHeight: 155,
    height: 155,
  },
}));

const CardTenantAtom = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { tenantId } = props;
  const classes = useStyles();

  const { loading, error, data } = useQuery(FIND_TENANT, {
    variables: { id: tenantId },
  });

  if (error) throw error;
  if (loading) return null;

  const tenant = data.findTenant;

  return (
    <Card ref={ref} className={classes.root}>
      <CardHeader title={`Tenant Registration ID`} subheader={tenant.name} />
      <CardMedia
        className={classes.media}
        component="img"
        image={`/assets/images/logos/${tenant.customer.logo}`}
        title={tenant.name}
        height={140}
      />
      <CardContent>
        <Grid container>
          <Grid item xs={12}>
            <Typography align="left">
              {`Customer Name: ${tenant.customer.name}`}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography align="left">
              {`Organization Name: ${tenant.organization.name}`}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography align="left">
              {`Admin Contact: ${tenant.customer.officialEmail}`}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
      <CardActions>{props.children}</CardActions>
    </Card>
  );
});
// Type and required properties
CardTenantAtom.propTypes = {
  tenantId: PropTypes.number.isRequired,
};
// Default properties
CardTenantAtom.defaultProps = {
  tenantId: 0,
};

export default CardTenantAtom;
