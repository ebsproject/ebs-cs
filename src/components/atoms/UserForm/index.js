import React, { useState } from 'react';
import {
  makeStyles,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import PageHeader from 'components/atoms/PageHeader';
//, validateOnChange = false, validate
export function useForm(initialFValues) {
  const [values, setValues] = useState(initialFValues);
  const [errors, setErrors] = useState({});

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
    /* if (validateOnChange)
            validate({ [name]: value })*/
  };

  const resetForm = () => {
    setValues(initialFValues);
    setErrors({});
  };

  return {
    values,
    setValues,
    errors,
    setErrors,
    handleInputChange,
    resetForm,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiFormControl-root': {
      width: '80%',
      margin: theme.spacing(1),
    },
  },
}));

export function Form(props) {
  const classes = useStyles();
  const { open, handleClose, onSubmit, children, title, description, ...other } =
    props;
  return (
    <Dialog
      fullWidth={true}
      maxWidth={'md'}
      open={open}
      onClose={handleClose}
      aria-labelledby='form-dialog-title'
    >
      <DialogTitle id='form-dialog-title'>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{description}</DialogContentText>

        <form
          className={classes.root}
          onSubmit={onSubmit}
          autoComplete='off'
          {...other}
        >
          {props.children}
          <DialogActions>
            <Button color='secondary' onClick={handleClose}>
              Close
            </Button>
            <Button type='submit'>Submit</Button>
          </DialogActions>
        </form>
      </DialogContent>
    </Dialog>
  );
}

Form.propTypes = {
  handleClose: PropTypes.object,
  onSubmit: PropTypes.object,
  open: PropTypes.bool,
  children: PropTypes.node,
  title: PropTypes.string,
  description: PropTypes.string,
};
// Default properties
Form.defaultProps = {
  handleClose: null,
  onSubmit: null,
  open: false,
  children: null,
  title: 'None',
  description: 'None',
};
