import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const EBSSplashScreenAtom = React.forwardRef((props, ref) => {
  return (
    <div className="flex flex-col h-screen justify-center items-center bg-ebs-splash">
      <div className="">
        <img
          className={'m-16'}
          width="128"
          src="assets/images/logos/EBS_V-W.svg"
          alt="logo"
        />
      </div>
      <div className="">
        <CircularProgress />
      </div>
    </div>
  );
});

export default EBSSplashScreenAtom;
