import React from "react";
import ko from "knockout";
import { ajaxSetup } from "@devexpress/analytics-core/analytics-utils";
import { AsyncExportApproach } from "devexpress-reporting/scopes/reporting-viewer-settings";
import { ActionId } from "devexpress-reporting/dx-reportdesigner";
import "devexpress-reporting/dx-reportdesigner";
import "./index.css";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
export default class ReportDesigner extends React.Component {
  constructor(props) {
    super(props);
    this.reportUrl = ko.observable(this.props.reportName);
    this.requestOptions = {
      host: process.env.REACT_APP_PRINTOUT_ENDPOINT,
      getDesignerModelAction: "ReportDesigner/GetReportDesignerModel",
    };

    this.callbacks = {
      CustomizeMenuActions: function (s, e) {
        var newMenu = e.GetById(ActionId.NewReport);
        var newViaWizardMenu = e.GetById(ActionId.NewReportViaWizard);
        var openMenu = e.GetById(ActionId.OpenReport);
        var designReportMenu = e.GetById(ActionId.ReportWizard);
        var localizationMenu = e.GetById(ActionId.Localization);
        var saveAsMenu = e.GetById(ActionId.SaveAs);
        var exitMenu = e.GetById(ActionId.Exit);

        if (newMenu) newMenu.visible = false;
        if (newViaWizardMenu) newViaWizardMenu.visible = false;
        if (openMenu) openMenu.visible = false;
        if (designReportMenu) designReportMenu.visible = false;
        if (localizationMenu) localizationMenu.visible = false;
        if (saveAsMenu) saveAsMenu.visible = false;
        if (exitMenu) exitMenu.visible = false;
      },
    };
  }
  render() {
    return <div ref="designer" data-bind="dxReportDesigner: $data"></div>;
  }
  componentDidMount() {
    ajaxSetup.ajaxSettings = {
      headers: {
        "Access-Control-Allow-Headers": "*",
        Authorization: "Bearer " + localStorage.getItem("id_token"),
      },
    };
    AsyncExportApproach(true);
    ko.applyBindings(
      {
        reportUrl: this.reportUrl,
        requestOptions: this.requestOptions,
        callbacks: this.callbacks,
      },
      this.refs.designer
    );
  }
  componentWillUnmount() {
    ko.cleanNode(this.refs.designer);
  }
}
