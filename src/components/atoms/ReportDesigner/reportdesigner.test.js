import React from "react";
// Component to be Test
import ReportDesigner from "./reportdesigner";
// Test Library
import { render, cleanup } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

// Props to send component to be rendered
const props = {
  reportName: "example",
};

test("Render correctly", () => {
  const { getByTestId } = render(<ReportDesigner {...props}></ReportDesigner>);
  expect(getByTestId("ReportDesignerTestId")).toBeInTheDocument();
});
