import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import queryString from 'query-string';
import EBSSplashScreen from 'components/atoms/EBSSplashScreen';
import { oAuthAuthentication } from 'store/ducks/auth';
import { showMessage } from 'store/ducks/message';

export default function CallbackView() {
  const dispatch = useDispatch();
  const storeLogin = useSelector((store) => store.login);

  useEffect(() => {
    const authResult = queryString.parse(window.location.search);

    // if (authResult.code != null && !storeLogin.isAuthenticated) {
    if (authResult.code != null) {
      dispatch(oAuthAuthentication(authResult.code));
    } else {
      dispatch(
        showMessage({
          message: `Not Login`,
          variant: 'success',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
        }),
      );
    }

    if (storeLogin.isAuthenticated) {
      window.location.href = '/';
    }
  }, [storeLogin, dispatch]);

  return <EBSSplashScreen />;
}
