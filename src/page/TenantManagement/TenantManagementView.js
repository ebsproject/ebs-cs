import React from "react";
import TenantManagement from "components/organism/Tenant";
import GridContainer from "components/molecule/Grid/GridContainer";
import GridItem from "components/molecule/Grid/GridItem";

export default function TenantManagementView(props) {
  return (
    <>
      <GridContainer>
        <GridItem xs={12}>
          <TenantManagement />
        </GridItem>
      </GridContainer>
    </>
  );
}
// Type and required properties
TenantManagementView.propTypes = {
 
};
// Default properties
TenantManagementView.defaultProps = {
   
};
