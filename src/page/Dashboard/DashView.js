import React, { useContext } from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { Typography, Card, CardContent } from '@material-ui/core';
import { InstanceProvider } from 'context/InstanceContext';
import {
  CardList as TenantList,
  Info as TenantInfo,
  InstanceList as TenantInstance,
} from 'components/molecule/Tenant';
import Products from 'components/molecule/ProductCardList';
import { TenantContext } from 'context/TenantContext';

function DashView() {
  const userInfo = useSelector(({ user }) => user);
  const tenantContext = useContext(TenantContext);

  if (!userInfo) return null;

  const { person: personInfo, tenants: tenantsInfo } = userInfo;
  const fullName = `${personInfo.familyName}, ${personInfo.givenName}`;
  const { tenantSelectedById, intanceSelectedById, domainSelectedById } =
    tenantContext.tenantState;

  return (
    <div className='flex flex-col items-center justify-center'>
      <Card className='w-full max-w-md'>
        <CardContent className='flex flex-col items-center justify-center p-32 text-center'>
          <Typography variant='h6' className='mb-4'>
            <FormattedMessage
              id='tnt.comp.comsoon.welcome'
              defaultMessage={`Welcome ${fullName} to Enterprise Breeding System (EBS)`}
            />
          </Typography>
          <img className='w-48 m-16' src='assets/images/logos/EBS.png' alt='logo' />

          {tenantsInfo.length && !tenantSelectedById && (
            <TenantList key='tenantList' tenants={tenantsInfo} />
          )}

          {tenantSelectedById && (
            <TenantInfo key='tenantInfo' tenantId={Number(tenantSelectedById)} />
          )}

          {tenantSelectedById && (
            <TenantInstance
              key='tenantInstanceInfo'
              tenantId={Number(tenantSelectedById)}
            />
          )}

          {tenantSelectedById && intanceSelectedById && (
            <Products key='productInfo' />
          )}
        </CardContent>
      </Card>
    </div>
  );
}

export default DashView;
