import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import UserManagement from './usermanagement'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<UserManagement></UserManagement>, div)
})
// Props to send component to be rendered
const props = {
  properyName: 'Value',
}
test('Render correctly', () => {
  const { getByTestId } = render(<UserManagement {...props}></UserManagement>)
  expect(getByTestId('UserManagementTestId')).toBeInTheDocument()
})
