import React from 'react'
 
import GridContainer from "components/molecule/Grid/GridContainer";
import GridItem from "components/molecule/Grid/GridItem";
import Users from "components/organism/Users"
//Your styles here
export default function UserManagementView(props) {
  /* This will be rendered in View
  @prop data-testid: Id to use inside usermanagement.test.js file.
 */
  return (
    <>
      <GridContainer>
        <GridItem xs={12}>
        <Users />
        </GridItem>
      </GridContainer>
    </>
  )
}
// Type and required properties
UserManagementView.propTypes = {

}
// Default properties
UserManagementView.defaultProps = {
 
}
