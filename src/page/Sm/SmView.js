import React from 'react';
import GridContainer from 'components/molecule/Grid/GridContainer';
import GridItem from 'components/molecule/Grid/GridItem';

export default function SmView(props) {
  // Props
  const { ...rest } = props;

  /*
  @prop data-testid: Id to use inside sm.test.js file.
 */
  return (
    <GridContainer>
      <GridItem xs={12}>
        <div id='sample_manager' />
      </GridItem>
    </GridContainer>
  );
}
// Type and required properties
SmView.propTypes = {};
// Default properties
SmView.defaultProps = {};
