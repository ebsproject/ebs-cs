import React from "react";
import PropTypes from "prop-types";
import Setting from "components/organism/Setting";
import GridContainer from "components/molecule/Grid/GridContainer";
import GridItem from "components/molecule/Grid/GridItem";

export default function SettingsManagementView(props) {
  /* This will be rendered in View
  @prop data-testid: Id to use inside settingsmanagement.test.js file.
 */
  return (
    <>
      <GridContainer>
        <GridItem xs={12}>
          <Setting />
        </GridItem>
      </GridContainer>
    </>
  );
}
// Type and required properties
SettingsManagementView.propTypes = {};
// Default properties
SettingsManagementView.defaultProps = {};
