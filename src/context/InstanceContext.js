import React, { useState, createContext } from 'react';
import { useSelector } from 'react-redux';

const InstanceContext = createContext();
const { Provider } = InstanceContext;

const InstanceProvider = ({ children }) => {
  const [instanceState, setInstanceState] = useState();
  const userName = useSelector((store) => store.user?.userName || '');
  const idUser = useSelector((store) => store.user?.id || 0);

  const setInstanceInfo = (domains) => {
    const domainsSpec = domains.map(({ context, domain, id, sgContext }) => ({
      domainId: id,
      name: domain.name,
      context,
      sgContext,
    }));

    const instanceSpecification = {
      userId: idUser,
      userName: userName,
      tenantId: Number(localStorage.getItem('tenant')),
      domainId: Number(localStorage.getItem('domain')),
      instance: {
        id: Number(localStorage.getItem('instance')),
        domains: domainsSpec,
      },
    };

    localStorage.setItem('auth', JSON.stringify(instanceSpecification));

    setInstanceState(instanceSpecification);
  };

  return (
    <Provider
      value={{
        instanceState,
        setInstanceState: (domainsInfo) => setInstanceInfo(domainsInfo),
      }}
    >
      {children}
    </Provider>
  );
};

export { InstanceContext, InstanceProvider };
