import React, { createContext, useState } from 'react';

const TenantContext = createContext();
const { Provider } = TenantContext;

const TenantProvider = ({ children }) => {
  const [tenantState, setTenantState] = useState({
    tenantSelectedById: localStorage.getItem('tenant'),
    intanceSelectedById: localStorage.getItem('instance'),
    domainSelectedById: localStorage.getItem('domain'),
  });

  const setTenantInfo = ({ tenantId, instanceId, domainId }) => {
    tenantId && localStorage.setItem('tenant', tenantId);
    instanceId && localStorage.setItem('instance', instanceId);
    domainId && localStorage.setItem('domain', domainId);

    setTenantState({
      tenantSelectedById: tenantId,
      intanceSelectedById: instanceId,
      domainSelectedById: domainId,
    });
  };

  return (
    <Provider
      value={{
        tenantState,
        setTenantState: (tenantInfo) => setTenantInfo(tenantInfo),
      }}
    >
      {children}
    </Provider>
  );
};

export { TenantContext, TenantProvider };
