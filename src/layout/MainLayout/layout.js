import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import {
  AppBar,
  Drawer,
  Toolbar,
  List,
  CssBaseline,
  Typography,
  Divider,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Icon,
  Collapse,
  LinearProgress,
} from '@material-ui/core';
import {
  AccessTime,
  ChevronLeft,
  ChevronRight,
  Dashboard,
  ExpandLess,
  ExpandMore,
  FilterList,
  Help,
  LensRounded,
  Search,
  Star,
  Apps,
  Menu,
} from '@material-ui/icons';
import DomainMenus from 'components/molecule/MenuDomainList';
import { FormattedMessage } from 'react-intl';
import AvatarProfile from 'components/atoms/AvatarProfile';
import { useQuery } from '@apollo/client';
import Client from 'utils/apollo';
import { FIND_DOMAIN, FIND_PRODUCT_LIST } from 'utils/apollo/gql/tenantManagement';
import { TenantContext } from 'context/TenantContext';
import EbsLogo from 'components/atoms/EBSLogoVertical';
import UserMessage from 'components/atoms/Message';
import Footer from 'components/molecule/Footer';

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  actions: {
    flexGrow: 1,
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('md')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
      display: 'block',
    },
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  logo: {
    marginLeft: 0,
    width: 250,
    height: 92,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  footer: {
    position: 'fixed',
    bottom: 0,
  },
}));

const MainLayout = React.forwardRef(({ component: Component, ...rest }, ref) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [domainInfo, setDomainInfo] = React.useState(null);
  const [products, setProducts] = React.useState(null);
  const [openApps, setOpenApps] = React.useState(false);
  const tenantContext = useContext(TenantContext);
  const { domainSelectedById } = tenantContext.tenantState;
  const history = useHistory();

  React.useEffect(() => {
    Client.query({
      query: FIND_DOMAIN,
      variables: { id: domainSelectedById },
      fetchPolicy: 'no-cache',
    })
      .then(({ data }) => {
        setDomainInfo(data.findDomain);
      })
      .catch(({ message }) => {});
    Client.query({
      query: FIND_PRODUCT_LIST,
      variables: {
        page: { number: 1, size: 100 },
        filters: [{ mod: 'EQ', col: 'domain.id', val: domainSelectedById }],
      },
      fetchPolicy: 'no-cache',
    })
      .then(({ data }) => {
        setProducts(data.findProductList.content);
      })
      .catch(({ message }) => {});
  }, [domainSelectedById]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleClick = (path) => {
    history.push(path);
  };

  const handleAppsClick = () => {
    setOpenApps(!openApps);
  };

  if (!domainInfo || !products) {
    return <LinearProgress />;
  } else {
    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position='fixed'
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar>
            <IconButton
              color='inherit'
              aria-label='open drawer'
              onClick={handleDrawerOpen}
              edge='start'
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <Menu />
            </IconButton>
            <Icon color='inherit' aria-label='open drawer' edge='start'>
              <img src={`/assets/images/domains/${domainInfo.icon}`} />
            </Icon>
            <Typography variant='h6' noWrap>
              <FormattedMessage
                id='sm.serviceManagement'
                defaultMessage={domainInfo.name}
              />
            </Typography>
            <div className={classes.actions}>
              <IconButton
                color='inherit'
                aria-label='open drawer'
                edge='start'
                href='/dashboard'
              >
                <Dashboard />
              </IconButton>
              <IconButton color='inherit' aria-label='open drawer' edge='start'>
                <Star />
              </IconButton>
              <IconButton color='inherit' aria-label='open drawer' edge='start'>
                <AccessTime />
              </IconButton>
              <IconButton
                color='inherit'
                aria-label='open drawer'
                edge='start'
                onClick={handleDrawerClose}
              >
                <FilterList />
              </IconButton>
            </div>
            <IconButton
              aria-label='show 17 new notifications'
              color='inherit'
              edge='end'
            >
              <Search />
            </IconButton>
            <IconButton
              aria-label='show 17 new notifications'
              color='inherit'
              edge='end'
            >
              <Help />
            </IconButton>
            <AvatarProfile />
          </Toolbar>
        </AppBar>
        <Drawer
          variant='permanent'
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.toolbar}>
            <EbsLogo className={classes.logo} />
          </div>
          <List>
            {products
              .slice()
              .sort(function (a, b) {
                return a.menuOrder - b.menuOrder;
              })
              .map((item, index) => (
                <ListItem
                  button
                  key={item.id}
                  onClick={() => handleClick(item.path)}
                >
                  <ListItemIcon>
                    {item.icon && (
                      <Icon
                        className='list-item-icon text-16 flex-shrink-0'
                        color='action'
                      >
                        {item.icon}
                      </Icon>
                    )}

                    {item.icon === null || (item.icon === '' && <LensRounded />)}
                  </ListItemIcon>
                  <ListItemText primary={`${item.name}`} />
                </ListItem>
              ))}
          </List>
          <Divider />
          <List>
            <ListItem button onClick={handleAppsClick}>
              <ListItemIcon>
                <Apps />
              </ListItemIcon>
              <ListItemText
                className={classes.listText}
                primary='Shortcut Domains'
              />

              {openApps ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openApps} timeout='auto' unmountOnExit>
              <List component='div' disablePadding>
                <DomainMenus />
              </List>
            </Collapse>
          </List>
          <div className={classes.toolbar}>
            <IconButton onClick={open ? handleDrawerClose : handleDrawerOpen}>
              {open ? <ChevronLeft /> : <ChevronRight />}
            </IconButton>
          </div>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <div>
            <Component {...rest} />
            <UserMessage />
          </div>
          <div className={classes.footer}>
            <Footer />
          </div>
        </main>
      </div>
    );
  }
});

export default MainLayout;
