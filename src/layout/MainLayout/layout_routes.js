import React from 'react';
import MainLayout from './layout';
import { ApolloProvider } from '@apollo/client';
import client from 'utils/apollo';

const LayoutRoutes = ({ ...rest }) => {
  return (
    <ApolloProvider client={client}>
      <MainLayout {...rest} />
    </ApolloProvider>
  );
};

export default LayoutRoutes;
