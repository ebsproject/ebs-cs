import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {loginStatus} from 'utils/enums';
import {useSignIn} from 'react-auth-kit';
import jwtService from 'services/jwtService';

function Loginlayout(props) {
  const storeAuth = useSelector((store) => store.auth);
  const storeUser = useSelector((user) => user.user);
  let history = useHistory();
  const signIn = useSignIn();

  useEffect(() => {
    if (storeUser.user && storeAuth.loginState === loginStatus.SUCCESS) {
      const userInfo = storeUser.user;

      const isLogin = signIn({
        token: jwtService.getIdToken(), //Just a random token
        tokenType: 'Bearer', // Token type set as Bearer
        authState: {
          userName: userInfo.userName,
          fullName: `${userInfo.person.familyName},${userInfo.person.givenName}`,
          jobTitle: userInfo.person.jobTitle,
        },
        expiresIn: 120, // Token Expriration time, in minutes
      });
      // console.log(storeAuth.token_id)
      //dispatch(UserActions.setUserData(storeAuth.token_id))

      if (isLogin) {
        history.push('/dashboard');
      } else {
        //  dispatch(actionAuth.changeLoginStatus(loginStatus.FAIL));
      }
    }
  }, [storeUser]);

  return <div>{props.children}</div>;
}

export default Loginlayout;
