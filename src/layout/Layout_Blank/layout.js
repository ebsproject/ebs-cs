import React, { useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  makeStyles,
} from "@material-ui/core/styles";
import { FormattedMessage } from "react-intl";
import {
  AppBar,
  CssBaseline,
  Toolbar,
  Typography,
  IconButton,
} from "@material-ui/core";
import {
  DashboardRounded,
} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    marginTop: 65,
    maxHeight: 800,
    padding: theme.spacing(1),
  },
}));

const LayoutOrganism = React.forwardRef((props, ref) => {
  const classes = useStyles();
  // Properties of the organism
  const [open, setOpen] = React.useState(false);

  return (
    <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar>
            <IconButton color="inherit" aria-label="open drawer" edge="start">
              <DashboardRounded />
            </IconButton>
            <Typography className={classes.title} variant="h6" noWrap>
              <FormattedMessage id="cs.dashboard" defaultMessage="Dashboard" />
            </Typography>
          </Toolbar>
        </AppBar>
        <main className={classes.content}>{props.children}</main>
    </div>
  );
});
// Type and required properties
LayoutOrganism.propTypes = {
  children: PropTypes.node,
};
// Default properties
LayoutOrganism.defaultProps = {
  children: null,
};

export default LayoutOrganism;
