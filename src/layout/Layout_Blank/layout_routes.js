import React from 'react';
import BlankLayout from './layout';
import { ApolloProvider } from '@apollo/client';
import client from 'utils/apollo';

const LayoutRoutes = ({ component: Component, ...rest }) => {
  return (
    <ApolloProvider client={client}>
      <BlankLayout>
        <Component {...rest} />
      </BlankLayout>
    </ApolloProvider>
  );
};

export default LayoutRoutes;
