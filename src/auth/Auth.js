import React, { useState, useEffect } from 'react';
import EBSSplashScreen from 'components/atoms/EBSSplashScreen';
import jwtService from 'services/jwtService';
import { useDispatch, useSelector } from 'react-redux';
import { setUserData } from 'store/ducks/user';
import { showMessage } from 'store/ducks/message';

function Auth(props) {
  const dispatch = useDispatch();
  const [waitAuthCheck, setWaitAuthCheck] = useState(true);

  const jwtCheck = () =>
    new Promise((resolve) => {
      jwtService.on('onAutoLogin', () => {
        /**
         * Retrieve user data
         */
        const codeDecoded = jwtService.getTokenDecoded();
        dispatch(setUserData(codeDecoded));

        resolve();
      });

      jwtService.on('onAutoLogout', (message) => {
        dispatch(
          showMessage({
            message: `${message}`,
            variant: 'error',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
          }),
        );
        resolve();
      });

      jwtService.on('onNoAccessToken', () => {
        resolve();
      });

      jwtService.init();
      return Promise.resolve();
    });

  useEffect(() => {
    return Promise.all([jwtCheck()]).then(() => {
      setWaitAuthCheck(false);
    });
  });

  return waitAuthCheck ? <EBSSplashScreen /> : <>{props.children}</>;
}

export default Auth;
