import React from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { Login } from 'page/Login';
import { Sm } from 'page/Sm';
import { Arm } from 'page/Arm';
import { Dashboard } from 'page/Dashboard';
import Settings from 'page/SettingsManagement';
import { TenantManagement } from 'page/TenantManagement';
import UserManagement from 'page/UserManagement';
import { Callback } from 'page/Callback';
import MainLayout from 'layout/MainLayout';
import BlankLayout from 'layout/Layout_Blank';
import { NotFound } from 'page/NotFound';

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path='/' render={(props) => <Login {...props} />} />
        <Route exact path='/callback' render={(props) => <Callback {...props} />} />
        <BlankLayout path={'/dashboard'} component={Dashboard} exact />
        <Route exact path='/cs/'>
          <Redirect to='/cs/tenantmanagement' />
        </Route>
        <MainLayout
          path={'/cs/tenantmanagement'}
          component={TenantManagement}
          exact
        />
        <MainLayout path={'/cs/usermanagement'} component={UserManagement} exact />
        <MainLayout path={'/cs/settings'} component={Settings} exact />
        <MainLayout path={'/ba/:route?'} component={Arm} exact />
        <MainLayout path={'/sm/:route?/:route?'} component={Sm} />
        <Route exact path='/not-found' render={(props) => <NotFound {...props} />} />
        <Redirect to='/not-found' />
      </Switch>
    </Router>
  );
};

export default Routes;
