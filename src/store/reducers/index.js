import { combineReducers } from 'redux';
import tenant from 'store/ducks/tenant';
import login from 'store/ducks/login';
import auth from 'store/ducks/auth';
import user from 'store/ducks/user';
import message from 'store/ducks/message';


const createReducer = asyncReducers =>
	combineReducers({
		auth,	
		login,
		tenant,
		user,
		message,
		...asyncReducers
	});

export default createReducer;
