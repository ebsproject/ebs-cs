import { client } from 'utils/apollo/apollo';
import { GET_PROFILE } from 'utils/apollo/gql/user';

// constants
const dataInitial = {};

const USER_SUCCESS = '[USER] LOAD USER SUCCESS';
const USER_FAILURE = '[USER] LOAD USER FAILURE';
const SET_USER_DATA = '[USER] SET USER DATA';
const USER_ROLES = '[USER] ROLES';

// reducer
export default function authReducer(state = null, action) {
  switch (action.type) {
    case SET_USER_DATA:
      return {
        ...state,
        ...action.payload,
      };
    case USER_ROLES:
      return {
        ...state,
        roles: action.payload,
      };
    case USER_SUCCESS:
      return {
        ...state,
      };

    default:
      return state;
  }
}

export const userSuccess = () => ({
  type: USER_SUCCESS,
});

export const userFailure = () => ({
  type: USER_FAILURE,
});

export const setUserProfile = (profile) => ({
  type: SET_USER_DATA,
  payload: profile,
});

// actions
export const setUserData = (tokenData) => async (dispatch, getState) => {
  const userName = tokenData['http://wso2.org/claims/emailaddress'].toLowerCase();

  await client
    .query({
      query: GET_PROFILE,
      variables: { val: userName },
    })
    .then((response) => {
      const profile = response.data.findUserList.content[0];

      if (profile) {
        dispatch(setUserProfile(profile));
        dispatch(userSuccess());
      }
    });
};
