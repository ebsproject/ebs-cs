import axios from 'axios';
import jwtService from 'services/jwtService';
import * as userActions from 'store/ducks/user';
import { loginSuccess, loginFailure } from 'store/ducks/login';
import { hideMessage, showMessage } from 'store/ducks/message';

// constants
const dataInitial = {};

// reducer
export default function authReducer(state = dataInitial, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export function oAuthAuthentication(authCode) {
  return (dispatch, getState) =>
    jwtService
      .signInWithWSO2(authCode)
      .then((result) => {
        const codeDecoded = jwtService.getTokenDecoded();

        //update user store
        dispatch(userActions.setUserData(codeDecoded));
        dispatch(loginSuccess());
      })
      .catch((error) => {
        console.log('Log error> ', error);
        dispatch(
          showMessage({
            message: ` ${error}`,
            variant: 'error',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
          }),
        );
        dispatch(loginFailure(error));
      });
}

const setSession = (access_token) => {
  if (access_token) {
    localStorage.setItem('access_token', access_token);
    axios.defaults.headers.common.Authorization = `Bearer ${access_token}`;
  } else {
    localStorage.removeItem('access_token');
    delete axios.defaults.headers.common.Authorization;
  }
};

const setToken = (id_token) => {
  if (id_token) {
    localStorage.setItem('id_token', id_token);
  } else {
    localStorage.removeItem('access_token');
  }
};
