import gql from 'graphql-tag';

// QUERIES

export const GET_ROLE_BY_ID = gql`
  query findUser($id: ID!) {
    findRole(id: $id) {
      id
      name
      productfunctions {
        id
        description
        product {
          id
          name
          domain {
            id
            name
          }
        }
      }
    }
  }
`;

export const GET_USER_BY_ID = gql`
  query findUser($id: ID!) {
    findUser(id: $id) {
      id
      userName
      person {
        givenName
        familyName
        jobTitle
        email
      }
      tenants {
        id
        name
      }
      roles {
        id
        name
        tenant
        description
      }
      functionalunits {
        id
        name
        tenant
      }
    }
  }
`;
export const GET_PROFILE = gql`
  # Write your query or mutation here
  query findUserList($val: String!) {
    findUserList(filters: { col: "userName", mod: EQ, val: $val }) {
      content {
        id
        userName
        tenants {
          id
          name
          instances {
            id
            name
            domaininstances {
              domain {
                name
                info
                icon
              }
              context
              sgContext
            }
          }
        }
        person {
          familyName
          additionalName
          givenName
          email
          gender
          jobTitle
          knowsAbout
        }
        roles {
          id
          name
          tenant
          description
        }
        functionalunits {
          id
          name
          tenantId
        }
      }
    }
  }
`;

export const GET_USERS_BY_TENANT = gql`
  # Write your query or mutation here
  query findTenant($id: ID!) {
    findTenant(id: $id) {
      id
      name
      users {
        id
        userName
        person {
          familyName
          givenName
          jobTitle
          knowsAbout
          language {
            id
            name
          }
        }
      }
    }
  }
`;

export const FIND_PERSON_LIST = gql`
  query findPersonList($number: Int!, $size: Int!) {
    findPersonList(page: { number: $number, size: $size }) {
      content {
        id
        giveName
        additionalName
        email
        officialEmail
        hasCredential
        jobTitle
        knowsAbout
        language {
          name
        }
        phone
        status
        addresss {
          region
          locality
          postalCode
          postOfficeBoxNumber
        }
      }
    }
  }
`;

// CREATE
export const CREATE_PERSON = gql`
  mutation createPerson($type: PersonInput!) {
    createPerson(PersonTo: $type) {
      id
    }
  }
`;
export const CREATE_USER = gql`
  mutation createUser($type: UserInput!) {
    createUser(UserTo: $type) {
      id
    }
  }
`;
// MODIFY
export const MODIFY_PERSON = gql`
  mutation modifyPerson($type: PersonInput!) {
    modifyPerson(PersonTo: $type) {
      id
    }
  }
`;

// DELETE
export const DELETE_USER = gql`
  mutation deleteUser($id: Int!) {
    deleteUser(iduser: $id)
  }
`;

export const DELETE_PERSON = gql`
  mutation deletePerson($id: Int!) {
    deletePerson(idperson: $id)
  }
`;
