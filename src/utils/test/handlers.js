import { graphql } from 'msw';

export const handlers = [
  graphql.query('findTenant', async (req, res, ctx) => {
    return res(
      ctx.json({
        data: {
          findTenant: {
            instances: [
              {
                domaininstances: [
                  {
                    domain: {
                      products: [
                        {
                          domain: { name: 'Core System' },
                          id: '21',
                          name: 'Settings',
                        },
                        {
                          domain: { name: 'Core System' },
                          id: '9',
                          name: 'User Management',
                        },
                        {
                          domain: { name: 'Core System' },
                          id: '10',
                          name: 'Tenant Management',
                        },
                        { domain: { name: 'Core System' }, id: '11', name: 'CRM' },
                      ],
                    },
                  },
                  {
                    domain: {
                      products: [
                        {
                          domain: { name: 'Core Breeding' },
                          id: '2',
                          name: 'Experiment Manager',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '4',
                          name: 'Planting Instruction Manager',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '5',
                          name: 'Pollination Instructions',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '3',
                          name: 'Harvest Manager',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '6',
                          name: 'Search Seeds',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '7',
                          name: 'Seed Inventory.',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '8',
                          name: 'KDX data transfer',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '1',
                          name: 'Experiment Creation',
                        },
                      ],
                    },
                  },
                ],
              },
              {
                domaininstances: [
                  {
                    domain: {
                      products: [
                        {
                          domain: { name: 'Core Breeding' },
                          id: '2',
                          name: 'Experiment Manager',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '4',
                          name: 'Planting Instruction Manager',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '5',
                          name: 'Pollination Instructions',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '3',
                          name: 'Harvest Manager',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '6',
                          name: 'Search Seeds',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '7',
                          name: 'Seed Inventory.',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '8',
                          name: 'KDX data transfer',
                        },
                        {
                          domain: { name: 'Core Breeding' },
                          id: '1',
                          name: 'Experiment Creation',
                        },
                      ],
                    },
                  },
                  {
                    domain: {
                      products: [
                        {
                          domain: { name: 'Service Management' },
                          id: '20',
                          name: 'Genotyping Service Manager',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '22',
                          name: 'Inspect',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '23',
                          name: 'Design',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '24',
                          name: 'Vendor',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '16',
                          name: 'Service Catalog',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '17',
                          name: 'PLIMS',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '18',
                          name: 'QLIMS',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '19',
                          name: 'Shipment Tool',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '14',
                          name: 'Request Manager',
                        },
                        {
                          domain: { name: 'Service Management' },
                          id: '25',
                          name: 'Create Request',
                        },
                      ],
                    },
                  },
                ],
              },
            ],
          },
        },
      }),
    );
  }),
  graphql.query('findProgramList', async (req, res, ctx) => {
    return res(
      ctx.json({
        data: {
          findProgramList: {
            content: [
              { id: '4', name: 'Admin Program' },
              { id: '1', name: 'Global Wheat Program' },
              { id: '2', name: 'Global Maize Program' },
              { id: '3', name: 'Durum Wheat' },
            ],
          },
        },
      }),
    );
  }),
];
