
export const setCurrentTenant = (currentTenant) =>{

    localStorage.setItem('current_tenant', currentTenant);
}


export const getCurrentTenant = () =>{

    return window.localStorage.getItem('current_tenant');
}