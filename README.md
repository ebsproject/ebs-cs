## Introduction

Microservices get a lot of traction these days. They allow multiple teams to work independently from each other, choose their own technology stacks and establish their own release cycles. Unfortunately, frontend development hasn’t fully capitalized yet on the benefits that microservices offer. The common practice for building websites remains “the monolith”: a single frontend codebase that consumes multiple APIs.

What if we could have microservices on the frontend? This would allow frontend developers to work together with their backend counterparts on the same feature and independently deploy parts of the website. Under single page application(SPA) we separate our app under modular domains, each of these modules are separate in data layer and view layer, conceptually and structurally. With atomic design and duck pattern we solve this need.

## How to register a module domain

First import the module; once you have your module running in serve mode.
We are goint to register our application inside public/index.html under systemjs-import map we need add a new line with this content:

```
    <script type="systemjs-importmap">
      {
        "imports": {
          "your-module-name": "http://localhost:PORT/module.bundle.js",
          "single-spa": "https://cdnjs.cloudflare.com/ajax/libs/single-spa/5.0.0/system/single-spa.min.js"
        }
      }
    </script>
``` 

We use the port where we are serving our module.

In the following script we import with 

```
      System.import('your-module-name').then(m => console.log(m))
```

to log modules life cycles

And finally register the application with single spa 

```
  singleSpa.registerApplication(
          "your_module_name",
          () => System.import("your_module_name"),
          (location) => location.pathname.startsWith("/paht")
        );
```

Now you can use in the route as you declare i.e. /app/new-module putting inside a div identified by module-name

```
 <div id="module-name"></div>
```
Now you mount the component and it render.

